---
layout: guide
title: Minecraft Bedrock Features and Setup
date: 2021-07-09T16:27:49.579Z
thumbnail: /images/uploads/geysermc.png
---
## How does Bedrock Version work in RammyCraft?

RammyCraft shares user data from Java with Bedrock user that has the same name by default. For example, assuming your name in Java version is **Steve,** you should use the same name (**Steve**) on Bedrock in order for seamless data sharing. You will have limited access to RammyCraft features when you first join until you link your Bedrock account with your Java account.

## Why the need to link account?

Due to the way Bedrock's UUID identification works differently than Java's UUID identification, account data sharing with plugins is not possible. Linking account will make your Bedrock account able to login as Java account. Players will be logged in as **BR_Steve** before linking and **Steve** after linking.

## What are the features available to unlinked Bedrock User?

Bedrock users will have limited access to certain features of RammyCraft in order to prevent account abuse. We still allow Bedrock user to access basic features before they link their account to reduce gameplay disruption as much as possible. 

List of available features:

* Login
* Economy
* Lock
* Shop
* Essentials

  * Back
  * Home
  * PvP

List of restricted features:

* Town

If you have a Java account with the same name before linking, you will share data of available features. Once you link your Java account, you will have access to restricted features.

![](/images/uploads/bedrock-unlinked.png)

## How to link my Bedrock Account with my Java Account?

1. Login to your java account and run /linkaccount \[bedrock_name]. Example, /linkaccount BR_Steve (Bedrock) for Steve (Java) 

  ![](/images/uploads/bedrock-linking.png)

2. Now login to your bedrock account and run the command shown by the instruction from Java account. Example, /linkaccount Steve 4933 (Java) for BR_Steve (Bedrock).
3. You should be kicked out of the Bedrock account, now login again and you should be logged in as your Java account. You should have acccess to all the features as if you are logged in as your Java account.

**You are not allowed to unlink your account after linking, please ensure you use the correct name to link your account. If you need to unlink, contact an admin to help you.**

Video tutorial can be found here <https://youtu.be/9-Qwn-OXlHc>.