---
layout: blog
title: Changes to Server Difficulty and Server Maintenance 3
date: 2020-04-27T01:22:12.015Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

I was made aware that the server difficulty was changed to **Hard**. This was a miscommunication between the admins and was made without going through proper channel before implement it. For that, I would like to apologize to all the players. To keep it fair for the rest of the players (present or future players), the difficulty will be locked at **Hard** for the forseeable future. This update post is served as a notice so that you all are aware of the change. 

On a side note, server will be going down on **Friday, 6PM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** and **the end world** will be reset during this time. Do move out your important items from both world before the reset to avoid losing any items. This will be the first phase of streamlining world reset and we are still deciding the frequency of resetting **the nether world** and **the end world.** I'm currently looking at resetting them both etiher when a new Minecraft version drop (Next reset will be at 1.16, 1.17 etc) or every 3 months for example. Let me know if you have any suggestions on the reset cycle.

There will be some gameplay enhancing features introduced after the maintenance so do look out for that :).

**Expected Downtime:** 6:00 PM - 6:30 PM\
**World Reset:** ExploreWorld, The End World

Regards, \
TomYaMee.
