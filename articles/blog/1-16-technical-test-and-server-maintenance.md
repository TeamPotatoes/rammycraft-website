---
layout: blog
title: 1.16 Technical Test and Server Maintenance
date: 2020-07-05T14:24:31.800Z
thumbnail: /images/uploads/netherupdateartwork.png
---
Dear RammyCrafters,

Server will be undergoing maintenance on the 7th July 2020. This maintenance will serve as a technical test for the upcoming 1.16 update. Server will be upgraded to 1.16 with the current world save. The main purpose of the update is to test the stability of the server before deploying the actual update to the server. The technical test will run from 7th July 2020 till 10th July 2020. If there is no major issue during the testing phase, the server will be fully deployed after. During this testing phase, the worlds will not be reset so none of the new addition will be available for you to enjoy just yet.

A snapshot of the server will be taken during the maintenance in case anything goes wrong with the update. Any progress made during the testing phase will be rolled back in case of any game breaking error. Otherwise, progress made will be carried forward to the actual update. 

Thank you for being patient with us while we work on updating the server. We are almost there!

Date: 7th July 2020\
Time: 8:30 AM to 9:00 AM (GMT+8)

Regards,\
TomYaMee.