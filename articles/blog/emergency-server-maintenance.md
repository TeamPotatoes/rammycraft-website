---
layout: blog
title: Emergency Server Maintenance
date: 2020-05-03T04:43:20.560Z
thumbnail: /images/uploads/6vogk3.png
---
Server will be under going emergency to fix some increasing chunk corruptions. The downtime will take somewhere around 30 mins max depending on how long it is going to take the fixer to fix the chunks.

## What will happen and what you should do?

The server file will be backed up prior to the downtime, this will take some time. The fixer will be ran on the current world file and attempt to fix any corrupted chunk with a backup from 1 May 2020 then another one from 22 April 2020. There might be some chunks that will fail to recover, in that case, the chunks will be deleted and regenerated. **THIS MAY OR MAY NOT BE WITHIN YOUR CHUNKS.** Please double check your builds and see if you are affected by this after the maintenance. **BACKUP IS AVAILABLE SO DO NOT WORRY**. Let the admin know and we will try our best to restore your affected builds.

**Time: 2PM PM to 2:30PM (Estimate)**\
**Affected Chunks:** 

* **x:5632 y:0 z:512 to x:6143 y:255 z:1023**
* **x:512 y:0 z:-5120 to x:1023 y:255 z:-4609**

Apologies for any inconvenience caused.

Regards,\
TomYaMee.
