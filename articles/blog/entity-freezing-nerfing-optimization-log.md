---
layout: blog
title: "Entity Freezing & Nerfing: Optimization Log"
date: 2020-05-18T05:22:50.710Z
thumbnail: /images/uploads/2020-05-18_12.14.31.png
---
*This post was last updated: 9th September 2021*

## Introduction

Minecraft servers have been getting increasingly harder to optimize in order to ensure smooth game play for all players. One of the main cause of server lag (TPS drop) is due to the amount of active entities in the server. This has gotten worse as more and more entities are introduced to the server and players require farms to acquire resources efficiently. This can cause the active entities to reach as many as 3000+ due to all the breeding that is happening. 

In order to optimize this without affecting too much of the vanilla experience the server is going for, we have find a compromise to handle these entities. One of the most conventional way to tackle this is to limit mob farms. By limiting amount of mobs in a single farm, the total entities count will drop significantly. Another way is to have mob stack on each other as shown below.

![StackMob Plugin](/images/uploads/mobstacker.png "StackMob Plugin")

 *Image credit to StackMob Plugin*

I find both of these methods to not be as vanilla as it can be. So we have decided to go with mob freezing and nerfing approach. This can make useless mobs (cows, pigs, sheep, etc) to consume as little as resources as possible without affecting the vanilla experience. **This blog post will go into details on how this approach work and will serve as a update log to keep track of what has been tweaked and altered in order to find the optimal settings.** To start off, let's start with the meaning of both freezing and nerfing.

## Mob Freezing

When a mob is frozen, the mob will lose all its memory on how to function as a living entity. They have no AI whatsoever; they won't move, they won't get pushed by water, they won't be affected by gravity (breaking blocks below them will just make them float), they won't perform entity specific functions like Sheep's wool regrowth. They are literally a soulless entity.

## Mob Nerfing

A nerfed mob is just mob that forgot how to move, their pathfinding algorithm has been disabled. However, they can still react to water movement, feeding, entity specific functions.

## How are mobs freezing and nerfing determined

There are 2 tasks being ran by the system: Passive task and critical task. Passive task is ran in interval while critical task is ran when server performance (TPS) drops below 18.5. They occasionally recover from their nerfed/frozen state when server gets more stable. You can also interact with the frozen mobs if you need them to recover from their state at any point of time.

## Current Behavior

* Tamed, leashed or love mode entities bypass all requirement and will never be frozen. Do not abuse this or we will have to set some threshold to freeze them as well

**Passive Task**

* Dense animal farms are **frozen** (Size of minimum 9 mobs in a densely packed space)
* Animal farms are **frozen** (Size of minimum 10 mobs in a sparse space)
* Sheep farms are **excluded** from being frozen. Sheep are useful for wool sheering automation so they are not frozen
* Sheep farms are **nerfed**. They don't need to path find when they are locked in a water loop for sheering, all they need is to be able to move in water.

**Critical Task**

* All farms will be frozen with scale-able requirement based on how bad the server performance is.
* Watermobs, ambient mobs and pillagers are completely **frozen**
* Sheep farms remain **excluded** from the freeze instead are **nerfed**

**Recommendation**

If you need farm to work for automation (eg: bee farm), try to limit the bees to at most 9 entities to ensure they don't get frozen/nerfed.

We will be constantly optimizing the freeze and nerf behavior so look out for this space for any changes made.

**Important**

Dropped items has a hard limit of 250 items at a time in radius of 5. If there are more than 250 items, the excess items will be removed every 2 minutes. Generally this should be fine unless you have 6 people die at the same area with all full inventory so keep this in mind.

## Optimization Log

9th September 2021

* Chicken farm has a limit of 100 chickens at within 4 chunks radius (Chicken farm overflowing)
* Dropped item has a limit of 1500 items within radius of 5 in main world (Automation overflowing)

20th June 2021

* View distance drop from 5 to 4, no-tick-view-distance remains at 7.
* Optimize entity spawn rate
* Lowered some entity tracking range
* Entity will not actively pathfind while blocks are broken
* Bob freezing behavior is now proactive instead of reactive

25th October 2020

* Identified issues with AFK mob farm overflowing dropped items, causing TPS and FPS drop. Set a hard limit of 250 dropped items in a radius of 5. Task are set to run every 2 minutes to clear area that have more than 250 items. 

10th June 2020

* Identified issues with spawner overloading mob spawns, installed Farm Limiter to limit mob spawner. Currently set to maximum of 40 mobs. Players tend to AFK at mob grinder, causing the mob to pile up (upwards of 400+ entities) lagging the server badly. This should also encourage players to be more active at their grinder rather than AFK the whole time.

6th June 2020

* Named entities will be frozen only if server TPS drops below 15

4th June 2020

* Freeze villager farm in sparse setting
* Remove villager optimizer, paper has better optimization now

2nd June 2020

* Named entities will now be frozen, a lot of villagers are named, which is the main source of lag.
* Lowered mob spawn per player from 25 to 20

21th May 2020

* There was an issue with the updated server software causing OOM, update to latest version to fix it
* Chunks loading should be smoother now
* Revert entitytracking tuning
* Disable Potato Town flow protection, seems like water ticking check is causing some lag. Probably due to creeper farms.
* Villagers pathfinding interval doubled, it will now take longer time to pathfind to look for bed/job site.
* Villagers, minecarts, hoppers and boats remain the highest lag factor

20th May 2020

* Slightly lowered mob spawn per player (These are all for natural spawning)
* Slightly lowered animal spawn per player (These are all for natural spawning)
* Tuned entitytracking when TPS drops
* Updated GC parameters
* Update server software

18th May 2020

* Add mob farms to passive freeze task
* Whitelist sheep farms from freezing but nerf them instead.