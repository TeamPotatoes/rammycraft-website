---
layout: blog
title: 1.16 Official Update and Server Maintenance
date: 2020-07-08T14:48:31.207Z
thumbnail: /images/uploads/netherupdateartwork.png
---
Dear RammyCrafters,

1.16 update will be officially dropping this Friday. Server will undergo maintenance from 8:30 AM and is expected to end at 9:30 AM. Do note that the ExploreWorld, nether world and the end world will be reset after the maintenance. Please ensure you have removed any important stuff you have in the affected worlds to avoid losing your items.

Date: 10th July 2020\
Time: 8:30 AM to 9:30 AM (GMT+8)

Regards,\
TomYaMee.