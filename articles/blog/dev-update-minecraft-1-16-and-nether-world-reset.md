---
layout: blog
title: 'Dev Update: Minecraft 1.16 and World Management Changes'
date: 2020-04-22T01:29:25.581Z
thumbnail: /images/uploads/280px-nether_update.png
---
Good day RammyCrafters, this post is served as a notice on the upcoming changes to the server. With the Nether update planned release in the first half of the year, we will be resetting the nether world for the new biomes as a way to start fresh with all the new and interesting addition to the game. At the same time, we will also be resetting the end world '**world_the_end**'.

## Justification

WorldBorder was introduced for the normal world '**world**' and '**ExploreWorld**' as a way to improve user experience as the region within the border was pre-generated pre-production to lessen the stress on the production server to generate new chunks when players explore. This was not introduced for nether world '**world_nether**' as the amount of content available for the nether world does not put stress on chunk generation hence there was no need to put a world border on it. This has allow players to explore the nether world endlessly, effectively making it hard for the new content to generate, thus the need to reset the world so the new content can be generated.

We have decided to reset the end world as well as there have been complains in the past about lack of loots due to old players looted them all. We have decided to take this opportunity to reset both the nether world and end world at the same time as a way to streamline world management process.

## What you need to do?

All you need to do is to ensure you have transferred your important stuff out of nether world and the end world before the world reset. As a side note, you shouldn't build anything or leave anything important in world that is not the main world '**world**'. As long as you can't protect your build, it is prone to changes.

## Going forward

We are looking to perform scheduled world reset for both '**world_nether**' and '**world_the_end**', probably once every 2 months. This is how the worlds of RammyCraft will be managed:

* Normal world: This will never get resets as this is the core and heart of all RammyCrafters, all your hard work and builds are (_read should_) in this world. There will be world border expansion in the future subject to amount of new content as well as server stability.
* Nether world: There will be scheduled reset, world border will not be introduced for the time being. We will have to see how much the new content is impacting server stability in terms of chunk generation.
* The end world: There will be scheduled reset, starting with ExplorerWorld reset ****scheduled early next month (May)
* ExploreWorld: Monthly scheduled reset, starting early next month. This world is act as a replacement for normal world reset for RammyCrafters to gather and explore new content that can not be generated in normal world due to border restriction.

In the meantime, hope everyone is doing well during this pandemic. Stay safe and practice good hygiene. 

Regards,\
TomYaMee.
