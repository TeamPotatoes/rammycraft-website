---
layout: blog
title: Server Maintenance 20
date: 2021-11-06T17:08:28.702Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Sunday, 10:30AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 7th November 2021\
**Expected Downtime:** 10:30AM - 11:00AM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.