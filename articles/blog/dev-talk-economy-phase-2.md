---
layout: blog
title: "Dev Talk: Economy Phase 2"
date: 2021-09-05T07:22:16.389Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Today I'm going to talk about server economy and the plan of transitioning to phase 2. Before I go into the details, it is important to know that the core aspect of RammyCraft has always been to make the gameplay as close as vanilla as possible. Economy has always been an afterthought to enhance certain aspect of gameplay. I'm also not an expert in economics or even doing business; I'm just a person that enjoys running and managing a server. So naturally there will be flaws when it comes to properly managing the economy of the server. 

A sensitive topic that is the server economy, I'm sure there will be disagreement from the players after reading this blog post. This is also why I'm writing this blog; to get feedback and discussion on where the economy of the server is heading towards as you as the players will be affected the most, so you should be the one to decide whether these decisions will affect you positively or negatively. 

## Why Economy In The First Place?

When the project was revived in 2017, we decided not to introduce economy to the server. Past experiences have taught me that badly managed economy can ruin the fun for the players. It wasn't until a year or two back, where we had to introduce new features like option to expand chunks, renaming town, or a way to procure building blocks or even rare items such as shulker shell (This was before shulker farm was a thing). These features should be locked behind some sort of wall to prevent players from abusing it (Spam buying chunks for no reason and not utilizing them; wasting claim chunks, renaming town anytime the players want). I have decided to re-introduce economy into the server as a way to perform transactions to easily access these features.

### Choice of Medium of Exchange

Different ideas were floated around to be used as the medium of exchange, in game items such as gold or emerald, points system or the good ol' virtual currencies. With each new release of Minecraft, farming these items have gotten easier and more efficient which I will go in depth later on. We ultimately decided to introduce a new virtual currency called "RammyCoins".

### Okay, now we have a economy in place, how should we generate money to spend?

In order for the economy to start functioning, we needed a way to inject money into the server. After several brainstorm, we decided to split the introduction of economy into 2 phases: Injection Phase and Self-Sufficient Phase.

#### Injection Phase

During this phase, admin shop was introduced to the server as a way for players to sell items to the server. As admin shop always has unlimited supply, this is where all the server economy is injected. The idea was to allow players to generate money this way through selling their haul like ores, blocks, farmable items in exchange for money.  

#### Self-Sufficient Phase

The goal of this phase (which is what we are transitioning into) is to transition from an admin shop oriented economy to player-owned economy. During this phase, players will setup their own shops, trade with other players, determine the value of the items themselves. This basically helps control the scarcity of the items in the server; thus having a economy set by the players rather having the admin shop printing money endlessly with its unlimited supply. 

## Controlling Inflation

In order to have a functioning economy, inflation needs to be controlled to prevent the collapse of the economy. Inflation means the injection of money into the server (Selling items in exchange for money) is way higher than the removal of money from the server (Buying items with money); causing the perceived value of the currency to drop. For example, a supply of 1,000,000 RammyCoins has more value than a supply of 1,000,000,000 RammyCoins. When everyone has abundance of money, economy in the server will need to adjust to maintain the value of the currency like increasing a building block price from 10 RammyCoins to 1,000 RammyCoins.

*We tried to control the inflation of the economy by reviewing and controlling the price of each items in the shop from time to time but..... we failed.*

### Economy in old Minecraft is not the same as in modern Minecraft

Back when Minecraft was still in its infancy, items farming or automation were not that popular or might not even exist. Inflation was easier to control back then due to this fact alone. That is no longer the case with each new release of Minecraft. New farm or automation methods are discovered left and right that have gotten ridiculously efficient to the point where you can just generate thousands of items by just AFK-ing. 

Generation of thousands of items without effort + Selling to admin shops = Massive injection of money into the server out of thin air without ***any effort.***

### How did we messed up

When we first introduced economy into the server, we thought about using some sort of dynamic pricing mechanism where it floats the value of an item up and down depending on the supply of the items at the time of sales. The supply floats randomly as well, high supply means lower value while low supply means higher value. 

We tried to control farmable items by disallowing selling of certain items such as iron ingots (golem farm), sugar cane (zero-tick exploits) while at the same time allowing sales of moderate farms such as foods (automation, villager farm), ores (quarry).

What we did not know was how easy it was for players to create 1 emerald trading farms. Players had been stockpiling bunch of emeralds and sell it to the admin shops this way. Bundling this with the random floating of items stock in the admin shops, players were able to capitalize on the flaw of the system and sold their emeralds when the supply of emerald in the admin shop was low. Few players became overnight millionaire this way.

Rollback was initially planned but we decided not to do it because it was my incompetency for not seeing this coming. Players merely capitalized on my own fault so snatching that away from them was very unfair to them.

#### Keeping track of any new farming method is a troublesome and tiring process

We tried to monitor and control prices of farmable items and I find that process too tiring and doing this every week or every 2 weeks is just not feasible. Cracks will form and a new farming method might go unnoticed for a while and this vulnerable period is when players can capitalize and cause unnecessary injection of money. Till this day, we are still finding new ways to farm items such as mathematically calculate the spawn locations of ores (which we removed the ability to sell to admin shop 2 weeks back, and recently we realized foods are also another potential problem (animal killing farms, chickens, cows, rabbit, etc, wheat, potatoes, sweet berries, wheat, potatoes, etc). These might not be a problem now but I foresee this will be an issue in the long run. So we decided bring the shop down now rather than later and start focusing on transitioning into phase 2.

## Vision for Phase 2

As mention previously, the plan of phase 2 is to transition from admin-oriented economy to player-owned economy. The goal that we'd like to achieve is:

* Encourage user to explore to earn money
* Encourage user to make effort to earn money
* Discourage unnecessary farms 
* Control inflation

### Encourage user to explore to earn money

Exploring should be rewarding when it comes to generating source of income. Exploration exclusive items should have high values due to the scarcity of it (Not farmable, can only be found if you explore)

### Encourage user to make effort to earn money

Players should be able to earn money for doing activities in the server. 

### Discourage unnecessary farms

Farmable items should not be source of money injection. This can help with controlling inflation as well as potentially reducing the amount of farms in the server; improving server performance as a side effect of having lesser farms.

## Plan for Phase 2

Here is what we plan to do for phase 2 to have a more balanced economy:

* Overhaul of items trading of admin shops
* Introduce ways for players to easily transact between players (player shop, trading, auction, etc)
* Adjust pricing of economy exclusive features such as town expansion, capture net

### Overhaul of items trading of admin shops

We will be removing the ability to sell all the farmable items to the shop. This will control the unnecessary injection of money into system. We are not entirely removing the ability to earn money from farming but instead we will move the source of income into a jobs system that we will be introducing in the next update. Players will be able to join certain jobs to earn money by performing activities in the server (eg: mining blocks, harvesting farms). This will encourage players to be active to earn rewards.

The shop will be mostly be a purchase shop where players can purchase building blocks to save themselves some time to grind for those blocks. The main injection of money will come from selling rare items from exploration. This is to encourage players to explore and find rare loot and cash in for a significant amount of money.

### Introduce ways for players to easily transact between players

This will be the core aspect of the economy phase 2, money earned from injection should cycle between players to encourage a healthy economy system. Players should be earning money from the existing supply instead of relying on injection of new money. We are looking to add ability to auction off items for a small fee or maybe even player made marketplace depending on how players are utilizing the economy. Since we are small community, having a easily accessible market anywhere and anytime through the auction system might be a better system for now.

### Adjust pricing of economy exclusive features

With the adjustment made to the ability to sell items on admin shops, source of income for players will be reduced. Hence, we will be reducing cost of economy exclusive features to be more in line with what we expect players to earn with the new economy. 

### To summarize

Source of money injection:

* Selling rare items
* Earning from Jobs doing various activities

Source of money burning:

* Tax from selling on auction
* Buying town expansion
* Town rename
* Buying capture net
* Ability to purchase global warps (To be confirmed)

Ways to trade:

* Transact through admin shop
* Transact through global auction
* Transact through player made marketplace (maybe)
* Trading system (maybe)

## Closing Thought

I have come to term with the fact that inflation will always be a thing and I can never solve it completely. All I can do is to reduce the intensity of the inflation. When everyone has too many money, economy doesn't make sense and won't work anymore. There will be no point selling items at that point as players will be able to afford anyway. And when everyone can just buy whatever they want, they will not have a goal to push for anymore and might get bored afterwards. As we are mainly a vanilla server, there is only so much things to do in Minecraft until you get bored and lost interest. Economy was a way to help prolong the gameplay loop of the server and I understand that this won't last forever as well. Ultimately, its the players choice of whether they want to see the economy succeed or just merely want to become the richest person in the server with nowhere to spend.

That's about what I have to say about this topic. I'm not an expert but I'm trying my best to create a fun environment for you to have fun and giving you a feeling of being a part of the RammyCraft community. Once again, I have to reiterate that I appreciate each and everyone of you for deciding to play on this server even though there are way better servers out there for you to enjoy. You guys have provided me a once in a lifetime experience of doing something I love and I hope I can keep this going for as long as I can.

Regards,\
TomYaMee.