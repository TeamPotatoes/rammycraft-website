---
layout: blog
title: Server Maintenance 18
date: 2021-09-02T10:14:47.116Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Friday, 5:00PM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 3rd September 2021\
**Expected Downtime:** 5:00PM - 5:30PM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.