---
layout: blog
title: Server Maintenance 8
date: 2020-09-03T12:06:53.538Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Friday, 8:30AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 4th September 2020\
**Expected Downtime:** 8:30AM - 9:00AM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.