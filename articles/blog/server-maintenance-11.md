---
layout: blog
title: Server Maintenance 11
date: 2020-12-02T10:18:34.856Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Saturday, 9:30AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. Server will be updated to 1.16.4 after the maintenance.

**Date:** 5th December 2020\
**Expected Downtime:** 9:30AM - 10:00AM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.