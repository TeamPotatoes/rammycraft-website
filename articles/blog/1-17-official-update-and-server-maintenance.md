---
layout: blog
title: 1.17 Official Update and Server Maintenance
date: 2021-07-06T09:09:05.800Z
thumbnail: /images/uploads/caves_-_cliffs_cover_art.png
---
Dear RammyCrafters,

1.17 update will be dropping as a technical test this Saturday. Server will undergo maintenance from 10 AM and is expected to end at 12 PM. Do note that the ExploreWorld, and the end world will be reset after the maintenance. Please ensure you have removed any important stuff you have in the affected worlds to avoid losing your items.

A snapshot of the server will be taken during the maintenance in case anything goes wrong with the update. Any progress made during the testing phase will be rolled back in case of any game breaking error. Otherwise, progress made will be carried forward to the actual update in a week or two.

Date: 10th July 2021\
Estimated Downtime: 2 hours\
Time: 10 AM to 12 PM (GMT+8)

Regards,\
TomYaMee.