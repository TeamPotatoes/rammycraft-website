---
layout: blog
title: Server Maintenance 21
date: 2022-01-05T13:08:49.054Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Saturday, 10:30AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld and End World** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

We are targeting to release 1.18.1 together with this maintenance so downtime will be longer than usual.

**Date:** 8th January 2022\
**Expected Downtime:** 10:30AM - 12:30PM\
**World Reset:** ExploreWorld, End World

Regards, \
TomYaMee.