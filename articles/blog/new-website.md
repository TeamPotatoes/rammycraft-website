---
layout: blog
title: New website
date: 2019-05-13T06:29:34.260Z
thumbnail: /images/uploads/1.png
---
We are happy to announce that we finally have new website! This website is designed and implemented from scratch using technologies such as React, Gatsby, Material Design Component, Netlify. The website is pretty much still working in progress as this is just the first iteration of the website. Feel free to provide us any feedback you have :)

The website also features blog posts. The blog posts will be used for us to publish Minecraft/RammyCraft related information. This is to allow consolidation of information so you can find all the information you need in one place.
