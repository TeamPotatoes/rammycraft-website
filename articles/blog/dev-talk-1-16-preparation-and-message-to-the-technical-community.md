---
layout: blog
title: "Dev Talk: 1.16 Preparation and Message to the Technical Community"
date: 2020-06-14T09:11:33.226Z
thumbnail: /images/uploads/280px-nether_update.png
---
Good day RammyCrafters,

Minecraft Nether Update (1.16) is nearing its release as Mojang is finalizing the build with the pre-releases and is expected to release the update by end of the month. This blog post is served as a reminder that the nether world will be reset once the 1.16 update drops in order for the new addition to be generated and allow players to explore them. Please make sure you have move out all your important stuff from the nether before the reset to avoid loss of items. For detailed explanation, refer to the previous blog post at [Dev Update: Minecraft 1.16 and World Management Changes](https://www.rammycraft.pw/blog/dev-update-minecraft-1-16-and-nether-world-reset/).

If you would like to get yourself up to date on the upcoming changes, check out the update post made by Mojang [here](https://www.minecraft.net/en-us/updates/nether).

## Message to the Technical Community

PaperMC, the server software that we are using to run the server has recently implemented a controversial exploit fix for TNT dupers in build #350. Here's the full patch notes:

```
Pistons invoke physics when they move blocks. The physics can cause
tnt blocks to ignite. However, pistons (when storing the blocks they "moved")
don't actually go back to the world state sometimes to check if something
like that happened. As a result they end up moving the tnt like it was
never ignited. This resulted in the ability to create machines
that can duplicate tnt, called "world eaters".
This patch makes the piston logic retrieve the block state from the world
prevent this from occuring.

Tested against the following tnt duper design:
https://www.youtube.com/watch?v=mS7xxNGhjxs

This patch also affects every type of machine that utilises
this mechanic. For example, dead coral is removed by a physics
update when being moved while it is attached to slimeblocks.

Standard piston machines that don't destroy or modify the
blocks they move by physics updates should be entirely
unaffected.

This patch fixes https://bugs.mojang.com/browse/MC-188840
```

It's already available as an update for 1.15.2 but I won't be updating it yet until 1.16 drops to give you guys some time to prepare for the change. This fix is baked into the server software without any configuration so this is something we will need to get used to when we update to 1.16.

Regards,\
TomYaMee.