---
layout: blog
title: Server Maintenance 12
date: 2021-01-06T12:38:07.913Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Friday, 5:30PM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 8th January 2021\
**Expected Downtime:** 5:30PM - 6:00PM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.