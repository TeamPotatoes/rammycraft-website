---
layout: blog
title: Server Maintenance 4
date: 2020-06-04T14:22:44.957Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Friday, 5:30PM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

There will be some gameplay enhancing features introduced after the maintenance so do look out for that :).

**Date:** 5th June 2020\
**Expected Downtime:** 1:00 AM - 1:30 AM *(Rescheduled)*\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.