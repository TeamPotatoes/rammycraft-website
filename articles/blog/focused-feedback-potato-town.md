---
layout: blog
title: 'Focused Feedback: Potato Town'
date: 2020-05-06T03:50:44.682Z
thumbnail: /images/uploads/1.png
---
Good day RammyCrafters, I'm trying out a new series of blog post called the **Focused Feedback** where I would like to focus on certain aspect of the server and improve upon them. For this entry, I would like to discuss about **Potato Town** as this was one of the early features implemented into the server. Being one of the features introduced as early as 2013, this has made a lot of the code base to be outdated and hard to maintain as the code were all over the place due to how inexperience I was back then. I would like to take this opportunity to slightly rework the code base and at the same time adding some new features to Potato Town. Let's go over the features that we have right now:

## Core Features

The core of Potato Town is to allow players to create, protect and manage their own town. As a strict no griefing server, this can help offload work from the admins to constantly needing to help protect and rollback griefs. The intended goal is to always protect anything and everything where ever its possible. A lot of time has past since 2013 (that's about 7 years), there are quite a numbers of addition to Minecraft through updates and I had to update the system to be compatible with all the new additions. This has led to the possibility of missing certain addition thus not being added to the system. The goal of this rework is to go through all the possible loopholes or so called **"bug"** and fix them so it can protect all your things as much as possible.

**This is where you can help by reporting any bugs, possible grief prone blocks or builds so I can look into it and fix it asap.** Few things that came to my mind are:

* Ignite protection from lightning
* Lava flow protection
* Drowned safe zone protection
* TNT protection
* Bees protection

## PR System

This was an addition to the core system after requests from various old players. It was implemented based on the feedback provided by them. As things stand, this might need a re-look and improvise as it may have been outdated by now. So far it only provide the PRs the ability to build and break blocks with the flexibility for the town leader to toggle them.

**What would you like to see be implemented for the PRs?** Few things I have in mind are:

* Allowing PRs to access chest
* Allowing PRs to teleport to their town

## Land Expansion Update

Recently we have introduced land expansion functionality to the system to allow players to buy more chunks to expand their land. The initial release had no limit on how many chunks a player can buy but this has raised a possibility that players can abuse and buy up thousands of chunks and claim everything. The ideal situation is player should always only claim land that they need but that may not always be the case. System abuser exists everywhere and is not exclusive to Minecraft. The only way is to find a compromise between the abuser and the legit players. 

As such, the v2 of land expansion was pushed last week with a limit of 30 purchases per town for now. This may change in the future depending on how is the feedback from the players. 30 purchases equate to 150 worth of chunks and that is quite a big land for you to work with. Remember the world is border restricted and you are sharing the land with other players. We must draw a line somewhere in order to not affect other players game play.

So far the feedback from the players are the price is too high which I can agree, this will definitely be adjusted soon. Economy is still very new to the server so it will take some time to work out the balance. (_I'm not a businessman so bear with me while I keep trying to fail at balancing this thing, also a reason why server is not profitable)._

## Some Scrapped Ideas

**Town Chat System**

There was a plan to introduce town chat system but it was put on hold as the server is small enough that I feel this system is just redundant to the already inactive chat. Besides, it is supposed to be a community server so have this scrapped should reduce the possibility of dramas and backstabbing from happening. I know players can still communicate it outside of Minecraft if they want but without having this in game is one less ways for them to have dramas. Having drama is the last thing I want in the server. 

**This may be an unpopular opinion for a silly reason but I would like to hear what you think and if this should still be implemented.**

**Town Nation System**

This feature was also scrapped after analyzing the need for it. Going through several popular plugins available on the marketplace, namely **Towny** as this was the inspiration for Potato Town, the main reason for Nation seems to be for Towny War purpose with some additional perks which can be done without Nation if need be. Having nations also make it easy to segregate the players making small communities from the already small community. 

**This is unlikely to be implemented as I don't see any significant benefit it can bring to the server.**

## Your Feedback is Appreciated

The whole purpose of this post is hoping to get some feedback from you guys. You guys as a player will have more experience than me when it comes to playing experience. You might see things that I don't see so having a 2-way feedback loop is beneficial for both you, the players and us, the developers.

Feel free to provide your feedback whenever you feel like it on either our [Facebook Group](https://www.facebook.com/groups/RammyCraft), [Discord](https://discord.gg/0aMO9RA1BmdMUrmx), or you may DM me as well. That's all for today's post, thank you for reading.

Regards,\
TomYaMee.
