---
layout: blog
title: Server Maintenance 7
date: 2020-08-05T09:34:32.686Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Friday, 5:30PM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 7th August 2020\
**Expected Downtime:** 5:30 PM - 6:00PM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.