---
layout: blog
title: Server Maintenance and Upcoming Host Change Test
date: 2020-05-21T11:23:48.256Z
thumbnail: /images/uploads/6vogk3.png
---
Attention Rammycrafters,

Server will be undergoing maintenance tomorrow to transfer the server to new host. We will be trying out the new host to see if it is any stable than the current server. Do take note of the potential IP change. **mc.rammycraft.pw should still work but it might take some time to update to the new server.** In case you can't access the server, you can try using **testserver.rammycraft.pw**.

Date: 22nd May 2020\
Time: 9am - 10:30 am

## Server Maintenance Completed

Server host migration is now completed. We will be testing the new host for a month to decide whether to stay with the current host or new host. Spec changes:

* 2GB RAM to 3GB RAM
* Better processing power
* Operation cost increased from 500 MYR per year to ~660 MYR per year