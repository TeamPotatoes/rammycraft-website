---
layout: blog
title: Server Maintenance 9
date: 2020-10-01T10:00:49.294Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Friday, 5:30AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. Server will be updated to 1.16.3 after the maintenance so make sure you have the latest version of Minecraft installed. With this update, the shop plugin is now out of date and will be disabled. We are evaluating new shop plugin and will be migrating to the new shop plugin over next few weeks.

**Date:** 2nd October 2020\
**Expected Downtime:** 5:30PM - 6:00PM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.