---
layout: blog
title: Server Maintenance 19
date: 2021-10-01T09:13:55.996Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Saturday, 10:00AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 2nd October 2021\
**Expected Downtime:** 10:00AM - 11:00AM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.