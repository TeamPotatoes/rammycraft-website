---
layout: blog
title: Server Maintenance 15
date: 2021-04-09T12:28:43.449Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Saturday, 10:30AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 10th April 2021\
**Expected Downtime:** 10:30AM - 11:00AM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.