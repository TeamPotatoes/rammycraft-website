---
layout: blog
title: Server Maintenance 14
date: 2021-03-06T12:53:58.768Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Sunday, 10:00AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 7th March 2021\
**Expected Downtime:** 10:00AM - 10:30AM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.