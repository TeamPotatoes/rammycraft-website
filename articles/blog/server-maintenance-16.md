---
layout: blog
title: Server Maintenance 16
date: 2021-05-07T09:51:48.530Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Saturday, 10:00AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 8th May 2021\
**Expected Downtime:** 10:00AM - 10:30AM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.