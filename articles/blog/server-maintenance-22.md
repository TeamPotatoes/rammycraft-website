---
layout: blog
title: Server Maintenance 22
date: 2022-03-05T15:50:22.417Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters, 

Server will be going down on **Saturday, 10:30AM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld and End World** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. 

**Date:** 6th March 2022\
**Expected Downtime:** 11AM - 11:30PM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.