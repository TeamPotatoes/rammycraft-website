---
layout: blog
title: Server Maintenance 10
date: 2020-11-05T01:16:12.036Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Dear RammyCrafters,

Server will be going down on **Friday, 5:30PM** for scheduled maintenance. During this downtime, server will be inaccessible. The **ExploreWorld** will be reset during this time. Do move out your important items from the world before the reset to avoid losing any items. Server shop will be re-introduced with a new plugin after this maintenance.

**Date:** 6th November 2020\
**Expected Downtime:** 5:30PM - 6:00PM\
**World Reset:** ExploreWorld

Regards, \
TomYaMee.