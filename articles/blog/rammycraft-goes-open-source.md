---
layout: blog
title: RammyCraft goes Open-Source
date: 2018-02-14T06:15:20.802Z
thumbnail: /images/uploads/osi_standard_logo.png
---
RammyCraft is now Open-Source meaning anyone can contribute/fork off the repository and make changes to the codebase of RammyCraft. As most of you know, RammyCraft is 90% self-developed using highly customized scripting plugin called Skript. This repository contains all Skript code that is currently running on the server.



https://gitlab.com/TomYaMee/rammycraft



First of all, I would like to thank all the RammyCrafters for the continuous support towards the server. Most of you probably already aware that the server is pretty much dead in terms of development as well as the player base. Just want to apologise and say that don't expect anymore further development to be made to the server. I simply has no time and motivation to continue to dedicate my time to work on the server. There are too many other important and/or interesting stuff for me to work on. The server was dead before and got brought back to life again due to the overwhelming support from you guys asking for a revival and I would like to thank you guys again for the support. I have no guarantee how long the server is gonna survive but I intend to keep it running as long as there is at least few players that still want to play on the server. I'm in a spot where I'm financially capable of keeping this server running as long as I want.



Just a short simple story of how I decided to choose Skript as my development tools for the server (If you are interested): It has always been my intention to start custom developing the server ever since I started my own server. The reason is simply because all the available plugins are too restrictive. I myself is a developer so it makes sense for me to prefer my own way of creating a customized system. Before going into Skript, I had look into maybe Java development as well however, Skript has a very low entry barrier which allowed me to easily pick up the scripting language and start working on the project. And that's basically why Skript was chosen.



As a final note, thank you for the support throughout the years. Managing this server has totally change my life and allowed me to meet and interact with all of you awesome people and I find that to be one of my best experience of my life. Long live RammyCraft!



Server Owner/Co-Founder signing off,\
TomYaMee.
