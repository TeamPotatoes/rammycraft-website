---
layout: blog
title: 'Dev Update: 1.14 Progress and Upcoming Changes'
date: 2019-06-10T05:46:04.272Z
thumbnail: /images/uploads/1-14-dev.png
---
How's it going RammyCrafters? Hope life is treating you guys well and you are enjoying the 10th anniversary of Minecraft. I see quite a few of you came back to the server to relive the nostalgia memories of Minecraft. Regardless of whether you are active players or returning players, remember that RammyCraft will always be there for as long as possible so you are always welcomed to join us anytime :D.

Back the the main topic of today's post, I would like to give a bit of updates regarding 1.14 update. Development and testing has been going well and it's ready to push to live server soon. However, as some of you are aware, with 1.14 update, the game is badly optimized which affects the server performance. Fixes are being pushed as fast as possible by the developers which minor version update v1.14.1 and v.1.14.2 being released so soon and we should be glad and appreciative of their work. As for RammyCraft, I'd like to wait until a stable version of 1.14 is released before pushing it to live. v.1.14.3 is already in pre-released branch so we will see how that works out but the update should happen really soon so be just a little bit more patient with us. :)

Several changes will be made to the server with the most notable change being **MOB GRIEFING WILL BE DISABLED.** Players have been complaining about how creeper explosion has made the game un-fun and while my vision for the server was to be as vanilla as possible, sometimes making compromises like these can benefit both the server and players more than the bad these changes bring. Besides that, the server being a community-based survival server, it makes sense to do this to allow players to showcase their creative builds without the need to worry about griefing by mobs. I hope this change can satisfy most of the players. Let me know what you think about this change.

Another changes to be made to the server is world border will be expanded to accommodate new update and allow new POIs to be generated. The current radius of the world is **7500** and it will be expanded to **15000** when 1.14 goes live. This is also another highly controversial change to the server and I would like to gather some opinion and feedback after playing 1.13 with such constraints in place. Is it the right choice to limit the border? Is removing the border better? Let me know! 

That's it for the dev update. Thanks for taking the time to read this and I'll see you guys next time.

Regards,\
TomYaMee.
