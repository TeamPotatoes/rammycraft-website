---
layout: blog
title: 1.17 and RammyCraft
date: 2021-06-12T03:11:25.232Z
thumbnail: /images/uploads/caves_-_cliffs_cover_art.png
---
Good day RammyCrafters! Minecraft 1.17 has officially dropped on the 8th of June 2021 and that means another update post on what and when you can expect from RammyCraft. Let's go through the changes that will be coming to RammyCraft.

## World Management

As the main cliff and cave update has been delayed to part 2 which will be released near Christmas, we will not be making any changes to the current main world setup. We are still unsure whether a reset is necessary due to the world height limit change as Mojang has not provided how the transition from old world to new world would be like. We remain optimistic that the transition will be smooth so expect an expansion of world border when 1.18 drops rather than a reset. The size of the expansion is to be determined, if you have any suggestion on how big it should be, let us know!

The end world as always will be reset as per tradition of resetting each major update. Be the first one to defeat the dragon again and explore the end city before any one else does!

## Bedrock Compatibility

Yes, we hear you mobile peeps that want to play on the server on the go. We will be starting our first testing of bedrock version compatibility in 1.17 utilizing the brilliant solution **GeyserMC**. The solution has been around for quite some time, we did consider adding it but it was not stable enough to be used on production server. So we decided to postpone the deployment and now we are reconsidering it after multiple players have ask for it. There are still some quirks and unfixable limitations that you need to be aware of when playing on a Bedrock version of Minecraft before reporting the issue to us. Please refer to <https://github.com/GeyserMC/Geyser/wiki/Current-Limitations>.

## New Shop Plugin

We will also be switching our shop plugin to a paid one which has more flexibility to perform what we need to do. There may also be some pricing changes after monitoring how players have been earning money to rebalance the economy. This plugin will also support selling a new item that will be releasing, read on next section for more info.

## Gotta Catch em' All

With the fixed border of main world, players have been finding difficulty to find biome dependent monsters (Bees, Pandas). We will be releasing a custom item called the **Capture Net** that allows user to capture certain monsters from ExploreWorld to bring it back to the main world. Currently it supports bees and pandas with more to be added based on players feedback so let us know what you would like supported. More information will be posted on the actual change log.

## Update Progress

We have also started our update process, keep track of them at <https://gitlab.com/TomYaMee/rammycraft/-/milestones/10>. That's all for now, thanks for reading and your continuous support to the server.

Regards,\
TomYaMee.