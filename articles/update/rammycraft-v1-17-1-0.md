---
layout: update
title: RammyCraft v1.17.1-0
date: 2021-08-01T16:24:04.759Z
thumbnail: /images/uploads/caves_-_cliffs_cover_art.png
---
Good day RammyCrafters! It's been almost a month since we dropped the 1.17 technical test that did not go as we planned as there were a bunch of crashing issues causing gameplay disruption. We are happy to announce that we are officially marking the update as stable! Thank you all for being patient and understanding with us while we worked through the issues. Here's the changelog for the update, enjoy!

## Bedrock Support

![](/images/uploads/geysermc.png "GeyserMC")

We are now officially supporting Bedrock users, players from mobile, Nintendo Switch, Xbox, and PlayStation can now join the server and play with the Java users. We have had players requesting for this as they want to play Minecraft on the go. Start playing with your Bedrock account by linking with your Java account today! Guide on how to do that can be found here: <https://rammycraft.teampotatoes.com/guide/minecraft-bedrock-features-and-setup/>. IP to join the server is: **bedrock.teampotatoes.com.**

## **Capture Net**

![](/images/uploads/capturenet.png "Capture Net")

We have had players asking for a way to bring monster back from ExploreWorld due to the limitation of portal not being able to bring monster back. Good news! We have sourced a way for you to capture monster from ExploreWorld back to the main world. 

Currently it will support capturing Panda and Bee with more monster supports coming. Our plan is to only allow post 1.13 mobs to be added to the support list due to the rarity of getting in the main world (Our main world is created with version 1.13). However, feel free to post any suggestion or concern if you think we should add support for all the other mobs as well.

We have also added optional resource pack to the server so you can see the capture net as an actual capture net rather than a lead (this is the base item that we used as capture net). Don't worry it won't affect the vanilla lead as this capture net uses specific NBT tags to be identified as a capture net. You will be prompt to download a resource pack the first time you join the server, you can choose to install or just ignore it if you prefer to not have extra resources to be loaded each time you join the server.

The item will be available to be bought from the server shop when we deploy the new shop plugin. Currently its in beta stage so if you find any bug please let us know!

## Shop

Shop is disabled for now as the plugin we used previously is no longer compatible, we have bought a plugin that should guarantee a longer update support so we don't have to keep switching shop plugin in the future. It will be back in the coming week or two. It will also be rebalanced with the recent discovery of new farms and mining methods.

## World Expansion

Regarding world expansion, as mentioned previously, we will be pushing the world expansion till 1.18 is dropped as there isn't any significant biome changes in 1.17. You can find all the new blocks in the ExploreWorld if needed as they are generated with version 1.17 and resets monthly.

## Chunk Corruption

We have detected chunk corruption at r.5.-1.mca which contains chunk sections 160,0,-32 to 191,15,-1. This region contains blocks 2560,0,-512 to 3071,255,-1. We have checked around before deleting the chunk and found no player builds so deleted it and regenerated it to fix the chunk corruption. If you had anything important inside that region please contact us for compensation. We have made a backup to check for any missing items if needed.

## Changelog

### Feature

PotatoTown:

* Prevent bedrock user from accessing town features unless linked

PotatoLogin:

* Implement support for bedrock data sharing with java data

PotatoEconomy:

* Implement bedrock support for vault integration

PotatoEssentials:

* Add bedrock support to link with java account.
* Commands affected:

  * Back
  * Spawn
  * Warp
  * Home
  * PvP
* Display bedrock for bedrock user chat and tab name

PotatoLock:

* Add bedrock support

PotatoChat:

* Add bedrock support

RammyAPI:

* Implement function to convert bedrock name to java name

### Fixes

PotatoLock:

* Prevent piston from pushing locked shulker

PotatoTown:

* Info player list showing inconsistent color for offline players
* Prevent tameable check for boat and minecart

### Plugins

Updated Plugins:

* LuckPerms
* ProtocolLib
* Multiverse
* LightAPI
* InteractionVisualizer
* SkinRestorer
* Themis
* skRayFall
* RammyLogin

Removed Plugins:

* HolographicsDisplay
* SAML
* skript-mirror
* WorldBorder
* BossShopPro

New Plugins:

* skript-reflect
* Farm Control
* Chunky
* ChunkyBorder
* Geyser
* Floodgate
* ShopGUIPlus
* NBTAPI
* RammyMob