---
layout: update
title: RammyCraft v1.17.1-1
date: 2021-08-20T17:34:06.848Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Good day RammyCrafters, we are dropping 1.17.1-1 update today mainly focuses on re-opening the spawn shop. Read on for the changes made to the shop:

## Plugin Switch

We are switching plugin from BossShopPro to a paid plugin ShopGUI+. As such, we are abandoning the spawn shop instead, you can now access the shop from anywhere and anytime in the world! Use **/shop** to access the new shop! We have ported all the items sold using the old plugin to the new plugin with some changes:

* Ores are now off the market due to the recently discovered method to mine ores efficiently, the market is now oversaturated with abundance of ores. To prevent further economy inflation, the merchants have decided to discontinue selling of ores in the shop.
* We have added a new category called the **TomYaMee Headhunting Service.** TomYaMee's headhunting service's business has been doing good and he has managed to signed an agreement with the merchants to allow him to sell his service directly from the shop. He will be looking to source for different type of heads either weekly or bi-weekly so look out for the Headhunting category for new selection of heads!
* With the shop reenabled, the Capture Net can now be purchased from the shop.

## Capture Net

![](/images/uploads/rammymob.gif "Capture Net")

You can now buy the capture net from the shop that allows you to bring mobs from the ExploreWorld back to the main world. The idea was to allow user to bring mobs released after Minecraft 1.14 due to the world border set in the main world, finding new mobs in the main world was difficult. It is in Beta stage so expect bugs. If you found any bugs do report back to us so we can fix it as soon as possible. Currently it supports Bees and Pandas but we will be adding more mobs into the support list once we have tested the stability of the plugin. 

## Quality of Life Update

### Potato Town

* You can now search town by player name. Use **/ptown info <town/player>** to view player's town info.

### PotatoEconomy

* You can now view how rich your friends are by using **/money <player>**

That's all for this update, a short update but hopefully you enjoy it until 1.18 drops. Happy Minecrafting!