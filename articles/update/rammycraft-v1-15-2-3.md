---
layout: update
title: RammyCraft v1.15.2-3
date: 2020-05-07T11:50:18.983Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
## Changelog:

* Fix crop trampling protection not working on wet farmland
* Fix /tpa message issue
* Fix messages not being sent out for new players
* Chunk buying price has been slashed by 50%, any chunks that have been bought after the v2 changes has been retroactively refunded to the players
* Reduced number of players required to skip night from 70% to 50%
