---
layout: update
title: RammyCraft v1.14.4-1
date: 2019-08-30T13:35:00.850Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Server has been updated through unscheduled maintenance to fix an issue with treasure chest crashing the server. The issue was related to treasure maps trying to locate treasures by chunk loading endlessly, causing the server to be unresponsive and crashed. A fix has been deployed to **remove treasure maps from all treasure chests** and replaced with other items as listed below:

* **Sunken ships:** Replaced with heart of sea, diamonds, books, or a blank map
* **Large ruins:** Replaced with chance to spawn diamond or heart of sea
* **Small ruins:** Removed completely.

This is a temporary fix to ensure the server doesn't crash from opening treasure chest. There's nothing we can do unless Mojang fixes this issue themselves. You can follow the issue at their official issue tracker here: 

<https://bugs.mojang.com/browse/MC-126244>\
<https://bugs.mojang.com/browse/MC-158088>

## Current State Update

Just a short update on current state of the server. ExploreWorld will remain disabled for a while as we are still trying to tweak the server to have best performance possible before re-introducing a new world again. We will be focusing on the economy aspect of the server next. We are looking to introduce simple admin shop first and let the players manage the economy of the server after. We will have a detailed post when we are ready to release this so stay tuned!
