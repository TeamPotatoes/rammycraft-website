---
layout: update
title: RammyCraft v1.14.2-0
date: 2019-06-18T05:44:27.856Z
thumbnail: /images/uploads/minecraft-1.14.png
---
Good day RammyCrafters, after an extensive testing and bug fixing, the server is now finally updated to 1.14! You are most welcomed to report issues or even suggest any improvement to the server on our [Facebook Group](https://www.facebook.com/groups/RammyCraft) or [RammyCraft Repo](https://gitlab.com/TomYaMee/rammycraft/issues). Enjoy the update!

## Plugins Changes

**Updated:**

* Skript
* ProtocolLib
* CoreProtect
* SkRayFall
* SkQuery
* Holographic Displays
* Multiverse Core
* SkinsRestorer
* Citizens
* DiscordSRV
* LuckPerms

**New Plugins:**

* skript-mirror
* MorkazSk

**Removed Plugins**

* ExtrasSK
* FAWE
* Skellett
* SkExtras
* SkUtilities
* Tablisknu

## Gameplay Changes

**Fixes:**

* Fix action bar to use native function
* Fix scheduled broadcast not working
* Fix crop trample not being protected from outsiders
* Fix double chest/trapped chest protection broken since 1.13. **Players are recommended to re-lock their chest! Do unlock both side of the chest to be safe.**
* Fix protected double chest not restoring properly from explosion
* Fix header and footer of tab list to use Bukkit API
* Fix duplication bug with protected dispenser and dropper
* Fix protected shulker box not restoring from explosion
* Fix town listing crashes
* Fix skeleton not despawning from safezone
* Fix phantom spawning in safezone
* Fix town name allowing illegal characters during creation
