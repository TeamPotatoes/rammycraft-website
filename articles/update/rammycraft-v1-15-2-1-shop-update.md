---
layout: update
title: RammyCraft v1.15.2-1 - Shop Update
date: 2020-02-21T16:23:52.133Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Good day RammyCrafter! The first iteration of economy has been released. You are most welcomed to report issues or even suggest any improvement to the server on our [Facebook Group](https://www.facebook.com/groups/RammyCraft) or [RammyCraft Repo](https://gitlab.com/TomYaMee/rammycraft/issues). Enjoy the update!

## Admin Shop (Early Access)

Admin shop has been added to the a small hut near the spawn, head over to check out the all new dynamic shop! The admin shop features dynamic pricing meaning the price will rise and fall randomly so be sure to buy and sell at the time you think is best! The higher the stock, the lower the price it drops and vice verse. Item being sold are not finalized, feel free to suggest what should and should not be added to the shop!

## ExploreWorld is back!

We have finally re-enabled ExploreWorld and it is currently generated based on 1.15.2 so its time to start your exploration and gather all the new blocks! Currently its reset cycle every month. Let us know if you think the cycle should be shorten!

**New:**

* **ExploreWorld is back!** Based on 1.15.2.
* You can now expand your town chunks by 5 (Price of 25000 Rammy Coins) using the command **/ptown buy chunk**
* You need to ensure your town has sufficient money by depositing to the town bank with **/ptown deposit <amount>**. Any town member can contribute to the bank.
* Admin Shop has been setup! Head over to spawn and check out. _\*Psst\* I have heard there's a need for music disc. Maybe the shop has some?_
