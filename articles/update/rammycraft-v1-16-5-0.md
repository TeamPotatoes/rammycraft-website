---
layout: update
title: RammyCraft v1.16.5-0
date: 2021-02-06T01:50:48.971Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Good day RammyCrafters,

The 13th server maintenance is now complete and server has been officially updated to 1.16.5. As per usual, server is under testing phase for a week to ensure that there is no game breaking issues. Server was backed up prior to the maintenance so we can roll back to the working state if there is any issue. Enjoy the new update!

Changelog

* MajoritySleep

  * Refactor sleep broadcast to use bossbar instead to declutter chat (Experimental). May have some UI bug but the logic still work the same.
  * Sleep trigger is now at 40%, reduced from 50%
* Essentials

  * Streamlined /tpa command. Running /tpa can now be optionally run with player name to directly request for teleportation. Usage:

    * /tpa - Opens TPA GUI
    * /tpa <player> - Request teleportation instantly
  * Clean up whisper message a little bit to make it less cluttered.
  * Fix whisper not working with /msg
* Town

  * Fix a critical bug where destroying a town does not properly remove town members data, causing data discrepancy when a user recreated the same town. A town will be missing claim chunk size due to this bug. If you are affected by this, please recreate your town with a different name to fix this issue. If you have deposited money into the town bank to upgrade your town, do contact an admin for a refund before you recreate your town.
* Lock

  * Optimize hopper detection, there were some cases where too many hopper minecart with chest on top of it (Automated farm) causing a lot of server lag. This optimization should hopefully help reduce the load.