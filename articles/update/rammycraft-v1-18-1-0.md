---
layout: update
title: RammyCraft v1.18.1-0
date: 2022-01-08T04:32:02.325Z
thumbnail: /images/uploads/minecraft-caves-and-cliffs-update-part-two-hero-image-01.webp
---
Good day RammyCrafters, after a month of waiting, we are finally ready to update to 1.18! As per tradition, we will be running 1.18 on trial/testing mode for a week or two to measure the performance before we commit to the update. There is a chance we will need to rollback to 1.17 if there are unforeseen issues.

As we are in testing mode at the moment, several features are disabled/limited and will be re-released once testing is done.

* Explore World is Disabled

  * Explore World will be disabled while we focus on monitoring the performance and resources usage solely on the main world. They will be back earliest next week.
* End World has not reset yet

  * End World is still on 1.17 version and will be reset earliest next week.
* Admin Shop is disabled

  * Admin shop is temporarily disabled while we wait and explore any potential new farms. This will be back at a later date

## World Management and Border Changes

As we all know, Minecraft 1.18 has introduced caves and cliffs which pushed Minecraft height limit to Y320 and Y-64 respectively. Good news! Mojang has worked their magic to make it compatible with existing world so we will not be resetting the world (*Your builds are safe!*). Any chunks that are in the current world border (8500 radius) will have their altitude extended downward retroactively. However you won't have access to new biomes and features. As such, we will be expanding the world border to 15000 radius so that you can explore or even settle your new home in the new biomes. **This change will be applied in a week or two!**

That's all for this update. Happy Minecrafting!