---
layout: update
title: RammyCraft v1.15.2-4 - Potato Town Update
date: 2020-05-11T06:37:41.618Z
thumbnail: /images/uploads/1.png
---
Good day RammyCrafters, a new update has been pushed to the server to resolve some of the issue faced by players. This update is focused on Potato Town as per my previous Focused Feedback post. Your town is now better protected than it has ever be!

## New

* **Implement liquid flow protection**

Your town will now be protected from griefers using liquid to destroy your build! We have consulted the server and has requested it to alter liquid flow physics. Lava and water will not flow from wilderness to your town now; lava and water can still flow from town to wilderness

![Liquid Flow Protection](/images/uploads/2020-05-11_14.43.03.png "Liquid Flow Protection")

* **Implement barrel protection**

Barrels in town are now protected from outsiders. Barrel protection has always been supported in Potato Lock. This update is to introduce the protection to Potato Town so you can protect barrels town wide with basic protection. _**Note that PotatoLock still provides you the best protection needed! (Explosion protection, user specific access permission, etc)**_

* **Implement ignite protection from lightning and trident**

We have enhanced town **Fire Flag** protection to further protect your town from lightning and trident ignition. Enabling **Fire** protection in your town will prevent lightning and trident from igniting your town and mobs!

* **Implement beacon, vehicle protection**

Vehicle and beacon is now protected from non town member. This includes any minecarts that has storage system and any boats. PRs will have optional flag to enable access, read more below.

* **Implement explosion protection flag**

Back in [v1.13.2-1](https://www.rammycraft.pw/update/rammycraft-v1.13.2-1), we have introduced creeper explosion protection if the explosion is within 1 radius of a town. This is now reworked as an optional flag! You can now enable/disable explosion by executing `/ptown flag explosion`. This will now affect both creeper explosion and newly implemented TNT explosion. This can prevent griefers from using tools like TNT canon from destroying your town from afar.

* **PR members can now interact in town with new PRFlag**

PR members in your town can now interact within your town with the flag `/ptown prflag interact`. This will allow them to interact and access any interact-able blocks like chests, doors, beacons, etc.

## Misc Changes

* PTown info flag color has been changed to whitelist system. <span style="color:red">Red means it is protected</span> while <span style="color:green">Green means it is allowed</span>
* Implement alias /t as an alternative to /ptown. You can now use /t as a replacement to /ptown
* Drowned will now despawn in Safezone
* Code optimization
* Rework system messages, now with more colors and better clarity on certain commands!
