---
layout: update
title: RammyCraft v1.15.2-2
date: 2020-05-01T10:17:11.598Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Good day RammyCrafters! The third server maintenance is now complete and we have brought some  game play enhancing features to the server! Let's go through the features; scroll to the bottom for TL;DR:

## World Reset and Ender Dragon

Explore World and The End World has been reset. This is the first phase of streamlining world reset schedule.  Head back in and start exploring the newly refreshed ExploreWorld or time to raid end cities and gather shulker shells! 

Another newly added feature is the chance for Ender Dragon to drop Elytra and Dragon Head. They have a very high chance to drop so its time to gather your end crystals and start getting those Elytra and Dragon Head that you have longed for!

## Shop Economy Re-balance

Spawn shop has been re-balanced with upper and lower boundary in place. The unlimited boundary price was not intended which has caused the server to be diluted with money. Keeping the server economy healthy is crucial to make sure whatever content we develop in the future can be fair and interesting to all players. Shop Economy was just introduced previous update so bear with us while we try to balance the mechanic as we grow.

## Majority Sleep Mechanic

Majority sleep vote has been implemented in "world", as long as there are majority of players sleeping in "world". Currently it's set to 70% majority of players so for example, 7 players need to be in bed if there is 10 players online for the time to past. The sleep mechanic works the same as vanilla so you can either skip night time or thunderstorm.

## Town Chunk Buying Mechanic Changes

Town chunk buying has been reworked to prepare for future extension to the chunk expansion mechanic. Currently you can only expand chunks by buying for a total of 30 times. Each upgrade will cost more than previous upgrades. Current price calculation is **`<chunk per purchase> * <chunk price> * (<amount of upgrades> + 1) * 0.75`**`. 

We are setting a limit as there is a risk of players abusing it and just buy up all the chunks and claim everything without actually utilizing them. I understand some players might not like this change as you might really need more chunks expansion. We are looking at more alternatives to introduce new ways to expand more chunks in the future.

## Phantom has been banished from world

Phantom has been one of the worst addition to the game in my opinion, it works fine in a single player world but it doesn't scale with multiplayer world as most of the time players don't need to sleep so players had difficulties resetting their insomnia status.

 Phantom spawning has been disabled in the normal world while keeping it spawning in ExploreWorld for those who want to farm phantom membrane.

## Server Upgrade

Server has doubled its capacity today in order to fix some lag issue. This means the server cost is now doubled as well but I'm hoping this can fix the lag the players has been experiencing. The server are running without any donation at the moment so if you like the server, you can consider donating to the server to help support the server running cost (Although there really isn't any interesting reward for donations yet as this is not a focus on the server).

## TL;DR:

* Rework town chunk buying mechanic
* Fix player not respawning at bed
* Fix tooltip not working for accepting town invites
* Fix item frame being able to be broken by other players
* Fix /back not working with /ptown home
* Fix wilderness title sometime display previous town's description
* Fix AFK announcer to be gender neutral
* Fix /home back behavior
* Implement TPS metrics to player list tab
* Implement majority sleep vote
* Killing ender dragon now has a chance to drop elytra (50%) and ender dragon head (25%)
* Phantom has been disabled in normal world
* Phantom remains available in ExploreWorld
* Changes to shop price boundary
* ExploreWorld + End reset
* Server has doubled its capacity to fix some lag issue

That's it for the update! Hope you enjoy these little changes and game play enhancement. Next update will probably be the 1.16 Nether Update which is expected to drop anytime between now and June. Server will be updated to 1.16 as soon as the server software and plugins are updated to support 1.16 so stay tuned for that. Enjoy your day in RammyCraft and stay safe during this pandemic!

Regards,\
TomYaMee.
