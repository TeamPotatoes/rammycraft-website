---
layout: update
title: RammyCraft v1.16.3-0
date: 2020-10-02T09:48:15.802Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Good day RammyCrafters,

The 9th server maintenance is now complete and server has been officially updated to 1.16.3. As per usual, server is under testing phase for a week to ensure that there is no game breaking issues. Server was backed up prior to the maintenance so we can roll back to the working state if there is any issue. Enjoy the new update!

Admin Shop is temporarily disabled as the plugin that we utilizes for the shop is now outdated and no longer works with 1.16.3. We will be evaluating alternative admin shop plugin and begin the migration process.

Changelog

* PotatoLock

  * Fix normal shulkerbox not restoring from explosion
  * Storage blocks (Shulkerbox, furnace, dropper, dispenser) will now restore to their original direction upon explosion restoration (Fixes #54)
  * Fix chest restoration not restoring tools' data (enchantments, durability, etc) (Fixes #79)
  * Add support for barrel, blast furnace and smoker
* PotatoChat

  * Fix link not opening correct link
* PotatoEssentials

  * Send PvP disabled message as action bar to declutter the chat
  * Increase AFK detection timer from 2 minutes to 5 minutes
* PotatoTown

  * Implement town rename (#61)
  * Fix damage cause checking to use string for comparison