---
layout: update
title: RammyCraft v1.14.4-2
date: 2019-09-06T10:21:50.764Z
thumbnail: /images/uploads/rammycraftlogo_textv2.png
---
Good day RammyCrafters, server maintenance is now completed with the re-introduction of daily server restart. With how unoptimized and unstable 1.14 is, the server's RAM just gets gobbled up easily causing the server to slow down. The only way to mitigate this is to restart the server to free the resources and keep the server fresh. Keep note of the restart schedule below:

**Frequency:** Daily\
**Time:** 4AM (GMT+8)

We will monitor the server performance and adjust the frequency as we see fit. Enjoy the small update!
