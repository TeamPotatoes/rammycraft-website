---
layout: update
title: RammyCraft v1.19.2-0
date: 2022-08-19T16:08:07.258Z
thumbnail: /images/uploads/vunj6kacfqyr4bl4yizkei.jpg
---
Good day RammyCrafters, server has been updated to 1.19! As per tradition, we will be running 1.19 on trial/testing mode for a week or two to measure the performance before we commit to the update. There is a chance we will need to rollback to 1.18 if there are unforeseen issues.

## Changelog

* Admin Shop re-enabled
* The End world has been reset
* ExploreWorld is re-enabled

### Plugin Updated

* CMLib
* InteractionVisualizer
* ProtocolLib
* BotSentry
* CoreProtect
* Chunky
* ChunkyBorder
* DiscordSRV
* FarmControl
* FarmLimiter
* NBTAPI
* Jobs
* LuckPerms
* QuickShop
* SkinsRestorer
* SkQuery
* SkRayFall
* Skript
* Themis
* Spark
* WorldEdit
* MorkazSk

### Plugin Removed

* SkVault
* LightAPI



That's all for this update. Happy Minecrafting!