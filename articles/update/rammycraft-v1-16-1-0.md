---
layout: update
title: RammyCraft v1.16.1-0
date: 2020-07-10T00:30:11.029Z
thumbnail: /images/uploads/netherupdateartwork.png
---
Good day RammyCrafters,

The time has finally come to officially introduce 1.16 to the server! The Nether Update was released on the 23rd of June 2020. As you may or may not know, updating the server is not as simple as flicking a switch. We need to wait for the server software to be updated as well as the plugins that we utilize on the server. We thank you for your patience while we waited for the 'relatively' stable release of the server software before finally dropping the update. For those who need a recap of the update, head over to [Minecraft Nether Update Site](https://www.minecraft.net/en-us/updates/nether/) for full update details. Now let's go through some of the changes to the server. For a full patch note, scroll to the bottom.

## World Management Changes

Earlier this year, I touched upon this [topic](https://www.rammycraft.pw/blog/dev-update-minecraft-1-16-and-nether-world-reset/) in anticipation of the Nether Update. After weighing different options and approaches that we can go with. We have decided to introduce a new world called the **ExploreWorld Nether**! 

This means the current nether world will be reset for the new addition to be generated for you guys to consume and enjoy. You can now build your creations or farms in the main world's nether and it will likely stay there for a foreseeable future! *That said, we have no plan of adding Potato Town support to nether yet.* No griefing protection will be available in the nether world. If you really want to build in the nether world, you can do so with the risk of getting griefed. We admins can help you roll back any griefs in the nether provided you report the grief within 45 days as that is the longest time we store the logs for.

Here's the overview of the tweaked world management changes:

* Main world remains untouched with border of 7500
* Main nether world is now reset and will remain untouched for the foreseeable future. The border is set at 3000 for now
* Main end world is now reset in line with the planned scheduled reset on major updates. This world will only reset whenever a big Minecraft update drops (eg: 1.16, 1.17, 1.18)
* ExploreWorld is now reset with the original schedule which is monthly reset
* ExploreWorld Nether is now introduced and is linked directly to ExploreWorld, resets monthly.

**INTRODUCTION OF EXPLOREWORLD NETHER IS EXPERIMENTAL AND WILL HAVE TO SEE HOW MUCH RESOURCE IT USES.** We may revert back to using the main nether world if the new world causes issues down the line.

## Good News for the Technical Community

Few weeks back I announced that the TNT duper will be fixed in 1.16 and has no way to revert the change due to it being a fix deployed to Paper ( the server ) directly. Good news, that is not the case anymore as the Paper team has added a config to re-enabled this back. Just to be clear with the RammyCraft community, we do not condone exploits and this is an exception as it is mainly used as an automated machine. TNT duper does not magically spawn blocks out of thin air as it only spawn entities which cannot be interacted with. Any exploits that spawn blocks out of thin air are not allowed and will be fixed eventually. This is also the official stance by Mojang and we respect that decision.

## PotatoTown Changes

Back in May, we had a [focused feedback](https://www.rammycraft.pw/blog/focused-feedback-potato-town/) topic about PotatoTown. We have made significant changes to the code base, cleaning up old code and improving them to make developing them in the future easier. We have also asked you guys to provide feedback on what we can improve on. I would like to thank all the people who have contributed in making the server a better place. Specially thanking:

* RaymondLee
* MilkTeaaaaa 
* cwkitkat
* issameZarone
* RedDragon
* Eddayy

for the suggestion and bug reports. Few important things to go through:

### Liquid Flow Protection Removed

Liquid flow protection was introduced in [v1.15.2-4](https://www.rammycraft.pw/update/rammycraft-v1-15-2-4-potato-town-update/). The intention was to prevent outsiders from pouring liquid into the player's town. This has caused some performance issues mainly due to creeper farms being a thing. The liquid flow event checks for every block the liquid flow to. Checking this event multiple blocks every tick (1/20 second) can be very taxing. This is unlikely to make a comeback unless we ban creeper farms which is not going to happen.

### More Protections and Fixes!

We have added a bunch of new protection and fixes to Potato Town thanks to the players! Check out the full patch note for detailed information and their relevant flags. We have plans to make the flag more customizable in the future but that will not be the priority for now.

## What’s next?

We will be focusing on completing the admin shop now that the 1.16 update is out. We will be looking at rebalancing all the items and as well as introducing new blocks to the shops. Several things that we would like to address such as introducing more building blocks that require unnecessary grinding for late game players for them to focus on buildings, introducing afk farmable items in shops to encourage buying from shops rather than setting up individual farms. We will talk about this in another post in the near future.

## Full Patch Note

### New

* Implement protection for name tagged mob from outsider
* Prevent portal creation if the portal teleportation is attempting to create a portal in a town not belonging to the player. Players can still use created portals. - under build flag
* Implement wither explosion protection - under explosion flag
* Implement protection against anvil, sweet berry bush, composter - under interact flag
* Prevent outsider from teleporting into town with chorus fruit - under interact flag
* Decluttered system messages for Potato Town, event messages are now sent through the action bar instead of spamming the chat.
* Rebuild RammyLogin, prevent same name from joining, prevent illegal character names from joining

### Fix

#### PotatoEssentials

* Fix majority sleep not excluding exploreworld (Already live)
* Fix console spam when player disconnect, re-optimize tab list update to 5 seconds from 1 second
* Improve auto AFK detection, it should work more reliably now hopefully its fully fixed

#### PotatoTown

* Fix non player entities not entering vehicles (Already live)
* Fix item frame being able to place anywhere (Already live)
* Fix town list owner always show as online
* Fix ignition flag not preventing lightning from transforming pigs and villagers. Bukkit API doesn't seem to support mooshroom transforms.
* Fix outsider can interact with bone meal
* Fix outsider being able to place boats or minecart - under build flag
* Fix outsider able to steal water mobs using water bucket - under interact flag
* Fix outsider being able to breed mobs - under interact flag
* Fix bell interaction for outsider - under interact flag
* Fix PR can ride mobs and interact with armor stand - under interact flag
* Fix toggleable blocks not protected - pressure plate, trapdoor, redstone items, lever, button, tripwire - under interact flag
* Prevent lectern book from being taken - under interact flag
* Fix outsiders able to break bed if part of the bed is outside the protected chunk - under break flag
* Fix outsider from breaking lilypads with boat - under break flag
* Fix armor stand not protected from outsider - under break flag
* Fix eating issue in Safezone
* Liquid flow protection has been removed, the event checks for every single water flow for their location. This can cause performance issues especially when users have a huge creeper farm.

#### PotatoLock

* Implement protection against hopper minecart

#### PotatoChat

* Fix link in chat

### Rework

* Remove auto restart script, handled through the server provider now
* Remove outdated plugin lists
* Decouple regex name check from TuSKe, use Java built in regex instead
* Decommission json.sk, use built in instead

Enjoy the new update and go out there and explore all the new biomes! We hope all the quality of life improvement that we have implemented here can make your experience in RammyCraft a more pleasant one.

Regards,\
TomYaMee and Atsukio.