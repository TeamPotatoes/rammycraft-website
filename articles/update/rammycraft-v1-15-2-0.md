---
layout: update
title: RammyCraft v1.15.2-0
date: 2020-02-01T05:51:52.133Z
thumbnail: /images/uploads/buzzybeesjava.jpg
---
Good day RammyCrafter! We are proud to announce that the server is now updated to build v1.15.2. You are most welcomed to report issues or even suggest any improvement to the server on our [Facebook Group](https://www.facebook.com/groups/RammyCraft) or [RammyCraft Repo](https://gitlab.com/TomYaMee/rammycraft/issues). Enjoy the update!

## Plugins Changes

**Updated:**

* skRayFall
* HologprahicDisplays
* CoreProtect
* WorldEdit
* LimitPillagers
* VillagerOptimiser
* Skirpt
* ProtocolLib
* SkinsRestorer
* LuckPerms
