---
layout: update
title: RammyCraft v1.13.2
date: 2019-02-06T04:33:10.325Z
thumbnail: /images/uploads/6vogk3.png
---
Good day RammyCrafter! We are proud to announce that the server is now updated to build v1.13.2. If you are not aware, server has been reset to address the diamond exploit issue mentioned previously. If you have submitted your build transfer request, please contact an admin as soon as possible to facilitate the transfer. As always, you can either report bugs found through our [GitLab](https://gitlab.com/TomYaMee/rammycraft/issues) repository, [Facebook Group](https://www.facebook.com/groups/RammyCraft/), or you can [email](tomyamee@rammycraft.pw) me directly.

* Several changes has been made to the behavior of mob spawning that you should be aware of:
* Spawn limit per player has been reduced to save memory (More on this later)
* Vacant chunks unload faster now to save memory
* Mob spawn rate tick rate increased from 1 to 2
* Mob spawn range has been reduced. This will help to condense mob spawn into smaller radius to mimic the appearance of normal spawn rates. While mob spawn in bigger range offers no advantage as user will still have to walk over to interact with the mob, reducing both spawn limit and spawn range will allow mobs to spawn nearer to player where it matters.
* Entity activation range has been reduced
* Item merge range has been increased. Do take note of this in case your exp orb or item merged and you couldn't find it.
* View distance is currently set to 6. Will observe and see if we can increase this in the future.
* Archer arrow despawn rate has been decreased. This will not affect players arrows.
* Explosion has been optimized
* Grass spread rate has been reduced
* Mob despawn range has been decrased, mob will despawn in a nearer range.
* Redstone computation and updates should be running more effeciently now using EigenCraft algorithm.

Have a nice day and happy minecrafting!
