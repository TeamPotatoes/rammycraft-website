# RammyCraft Website v3

[![coverage report](https://gitlab.com/TeamPotatoes/rammycraft-website/badges/master/coverage.svg)](https://gitlab.com/TeamPotatoes/rammycraft-website/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/c9504c9d-9765-4f5b-b1df-cd08a0fc3126/deploy-status)](https://app.netlify.com/sites/rammycraft/deploys)

# Demo

Live at : [https://www.rammycraft.pw](https://www.rammycraft.pw)

# Usage

## Development

#### Step 0: Prerequisites 

* Install [Yarn](https://yarnpkg.com/lang/en/)
* Install [NodeJS](https://nodejs.org/en/)

#### Step 1: Setup Dev Env

```
npm install -g gatsby-cli
git clone https://gitlab.com/TeamPotatoes/rammycraft-website.git
cd rammycraft-website
yarn install
```

#### Step 2: Config SASS Path

```
SASS_PATH=.\node_modules
```

#### Step 3: Run Dev

```
gatsby develop
```

#### Step 4: Build

```
gatsby build
```
