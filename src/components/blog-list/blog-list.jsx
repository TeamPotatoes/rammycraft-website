import React from "react"
import PropTypes from "prop-types"

import BlogCard from "../card-blog/card-blog"
import { Link } from "gatsby"
import Pagination from '@material-ui/lab/Pagination'
import PaginationItem from '@material-ui/lab/PaginationItem'

import { makeStyles } from '@material-ui/core/styles'
import "./blog-list.scss"

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
    '& > * > ul': {
      justifyContent: 'center'
    }
  },
}))

const BlogList = (props) => {

  const classes = useStyles()

  return (
    <React.Fragment>
      {props.content.map((edge, index) => {
        return (
          <div
            className={
              "mdc-layout-grid__cell mdc-layout-grid__cell--span-12"
            }
            key={edge.node.id}
          >
            <BlogCard
              title={edge.node.frontmatter.title}
              excerpt={edge.node.excerpt}
              date={edge.node.frontmatter.date}
              thumbnail={edge.node.frontmatter.thumbnail}
              slug={edge.node.fields.slug}
            />
          </div>
        )
      })}
      <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12 center">
        <div className={classes.root}>
          <Pagination
            page={props.pageContext.currentPage}
            count={props.pageContext.numPages}
            color="primary"
            renderItem={(item) => (
              <PaginationItem
                component={Link}
                to={`/${props.pageContext.type}/${item.page === 1 ? '' : item.page}`}
                {...item}
              />
            )}
          />
        </div>
        
      </div>
      
    </React.Fragment>
  )
}

BlogList.propTypes = {
  content: PropTypes.array,
}

export default BlogList
