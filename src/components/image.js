import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `StaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `StaticQuery`: https://gatsby.dev/staticquery
 */

const Image = (props) => (
  <StaticQuery
    query={graphql`
      {
        images: allFile(filter: { extension: { ne: "md" } }) {
          edges {
            node {
              relativePath
              name
              extension
              childImageSharp {
                fluid(maxWidth: 2000) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    `}
    render={({ images }) => {
      const image = images.edges.find((n) => {
        return n.node.relativePath.includes(props.filename)
      })
      if (!image || image.node.extension === "svg") {
        return null
      }

      const fluid = image.node.childImageSharp.fluid
      return <Img alt={props.alt} fluid={fluid} />
    }}
  />
)

Image.propTypes = {
  filename: PropTypes.string,
  alt: PropTypes.string,
}

export default Image
