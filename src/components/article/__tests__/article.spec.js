import React from "react"
import renderer from "react-test-renderer"
import Article from "../article"
describe("Article", () => {
  it("renders correctly", () => {
    const tree = renderer.create(<Article>Test Article</Article>).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
