import React from "react"
import PropTypes from "prop-types"

import "./article.scss"

const Article = props => (
  <div className="mdc-typography--body1 article-text">{props.children}</div>
)

Article.propTypes = {
  children: PropTypes.any,
}

export default Article
