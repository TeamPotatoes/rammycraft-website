import React from "react"
import PropTypes from "prop-types"

import "./button.scss"

const Button = props => (
  <button
    className={"mdc-button mdc-button--raised rammy-button" + props.className}
    onClick={props.onClick}
  >
    {props.label}
  </button>
)

Button.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
}

export default Button
