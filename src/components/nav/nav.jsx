import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { MDCTopAppBar } from "@material/top-app-bar/index"
import Image from "../image"

import "./nav.scss"

const Nav = (props) => {
  const [darkMode, setDarkMode] = useState(false)
  const [scrolled, setScrolled] = useState(false)

  function handleClick(e) {
    e.preventDefault()
    let menu = document.querySelector(".menu")
    if (menu.classList.contains("menu-show")) {
      menu.classList.remove("menu-show")
    } else {
      menu.classList.add("menu-show")
    }
  }

  function handleToggleDarkMode() {
    localStorage.setItem("darkMode", !darkMode)
    setDarkMode(!darkMode)
  }

  useEffect(() => {
    let topAppBarElement = document.querySelector(".mdc-top-app-bar")
    new MDCTopAppBar(topAppBarElement)

    var gatsby

    if (typeof window !== "undefined") {
      setDarkMode(localStorage.getItem("darkMode") === "true")
      gatsby = document.getElementById("___gatsby")
    }
    if (darkMode) {
      gatsby.classList.add("dark")
    } else {
      gatsby.classList.remove("dark")
    }
  }, [darkMode])

  useEffect(() => {
    window.onscroll = function () {
      if (Math.round(window.scrollY) > 100) {
        if (!scrolled) setScrolled(true)
      } else {
        if (scrolled) setScrolled(false)
      }
    }
    window.dispatchEvent(new CustomEvent("scroll"))
  })

  return (
    <header
      className={
        "mdc-top-app-bar mdc-top-app-bar--fixed nav" +
        (props.isIndex ? " nav-index" : "") +
        (scrolled ? " mdc-top-app-bar--fixed-scrolled nav-scrolled" : "")
      }
    >
      <div className="mdc-top-app-bar__row">
        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start nav-logo">
          <Link to="/" className="no-decoration-link" activeClassName="active">
            <div className="logo-container">
              {darkMode ? (
                <Image filename="rammycraftlogo_whiteborder.png" />
              ) : (
                <Image filename="rammycraftlogo.png" />
              )}
            </div>
            <span className="mdc-top-app-bar__title nav-title">
              {props.siteTitle}
            </span>
          </Link>
        </section>
        <nav className="nav-mobile">
          <div>
            <i
              className="material-icons fs-24 nav-icon"
              onClick={handleToggleDarkMode}
            >
              brightness_3
            </i>
            <button
              href="#"
              className="mdc-icon-button material-icons mdc-top-app-bar__navigation-icon--unbounded nav-icon"
              aria-label="Menu"
              alt="Menu"
              onClick={handleClick}
            >
              menu
            </button>
          </div>
        </nav>
        {/* Desktop Navigation */}
        <nav className="nav-desktop">
          <ul>
            <li>
              <Link to="/" className="mdc-typography--subtitle1">
                Home
              </Link>
            </li>
            <li>
              <Link to="/guide" className="mdc-typography--subtitle1">
                Guide
              </Link>
            </li>
            <li>
              <Link to="/blog" className="mdc-typography--subtitle1">
                Blog
              </Link>
            </li>
            <li>
              <Link to="/update" className="mdc-typography--subtitle1">
                Update
              </Link>
            </li>
            <li>
              <Link to="/about" className="mdc-typography--subtitle1">
                About
              </Link>
            </li>
            <li onClick={handleToggleDarkMode}>
              <i className="material-icons fs-24">brightness_3</i>
            </li>
          </ul>
        </nav>
      </div>

      {/* Mobile Navigation */}
      <div className="menu">
        <ul>
          <li>
            <Link
              to="/"
              className="mdc-typography--subtitle1"
              activeClassName="active"
            >
              Home
            </Link>
          </li>
          <li>
            <Link
              to="/guide"
              className="mdc-typography--subtitle1"
              activeClassName="active"
            >
              Guide
            </Link>
          </li>
          <li>
            <Link
              to="/blog"
              className="mdc-typography--subtitle1"
              activeClassName="active"
            >
              Blog
            </Link>
          </li>
          <li>
            <Link
              to="/update"
              className="mdc-typography--subtitle1"
              activeClassName="active"
            >
              Update
            </Link>
          </li>
          <li>
            <Link
              to="/about"
              className="mdc-typography--subtitle1"
              activeClassName="active"
            >
              About
            </Link>
          </li>
        </ul>
      </div>
    </header>
  )
}

Nav.propTypes = {
  siteTitle: PropTypes.string,
}

export default Nav
