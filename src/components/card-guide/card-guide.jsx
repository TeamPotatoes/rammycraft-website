import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"

import "./card-guide.scss"
import Image from "../image"

const GuideCard = (props) => {
  let title = props.title.toLowerCase()
  let filename = props.thumbnail.split("/images/")[1]

  return (
    <Link
      to={props.slug}
      className="no-decoration-link"
      activeClassName="active"
    >
      <div className="mdc-card mdc-card--outlined guide-card">
        <div className="mdc-card__primary-action" tabIndex="0">
          <div className="mdc-card__media">
            <Image filename={filename} />
          </div>

          <div className="blog-card-container">
            <h2 className="mdc-typography mdc-typography--headline6">
              {props.title}
            </h2>
            <div className="mdc-typography mdc-typography--body2">
              {props.excerpt}
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

GuideCard.propTypes = {
  title: PropTypes.string,
  thumbnail: PropTypes.string,
  date: PropTypes.string,
  excerpt: PropTypes.string,
  slug: PropTypes.string,
}

export default GuideCard
