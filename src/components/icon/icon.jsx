import React from "react"
import PropTypes from "prop-types"

import "./icon.scss"

import Image from "../image"
const Icon = props => (
  <div className="icon-container" key={props.id}>
    <a
      href={props.href}
      target="_blank"
      rel="noopener noreferrer"
      className="footer-icon-container"
    >
      <Image filename={props.filename} />
    </a>
  </div>
)

Icon.propTypes = {
  href: PropTypes.string,
  filename: PropTypes.string,
}

export default Icon
