import React from "react"
import PropTypes from "prop-types"
import Image from "../image"

import "./banner.scss"

const Banner = props => (
  <div className="banner">
    <Image filename={props.filename} />
  </div>
)

Banner.propTypes = {
  filename: PropTypes.string,
  alt: PropTypes.string,
}

export default Banner
