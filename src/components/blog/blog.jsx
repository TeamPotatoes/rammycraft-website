import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { Disqus } from "gatsby-plugin-disqus"

import Image from "../image"

import "./blog.scss"

const Blog = (props) => {
  let date = new Date(props.date)
  let disqusConfig = {
    url: `${props.siteUrl}`,
    identifier: props.title,
    title: props.title,
  }

  return (
    <div className="blog-container">
      <div className="blog-post">
        <Link
          to={`/${props.backlink}`}
          className="no-decoration-link back-button"
        >
          ☜(ˆ▿ˆc) &nbsp;Back to previous page
        </Link>
        <h1 className="mdc-typography--headline1 headline1">{props.title}</h1>
        <h2 className="mdc-typography--subtitle2 subtitle2">
          <i className="material-icons fs-24 v-align">access_time</i> &nbsp;
          {date.toDateString()} {date.toLocaleTimeString()}{" "}
          {date.toString().match(/GMT.*$/)[0]} &nbsp;{" "}
          <i className="material-icons fs-24 v-align">library_books</i>{" "}
          {props.timeToRead} Min(s) Read
        </h2>
        <div className="blog-post-image-container">
          <Image filename={props.thumbnail} />
        </div>
        <div
          className="blog-post-content"
          dangerouslySetInnerHTML={{ __html: props.html }}
        />
        <Disqus config={disqusConfig} />
      </div>
    </div>
  )
}

Blog.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  thumbnail: PropTypes.string,
  html: PropTypes.string,
  timeToRead: PropTypes.number,
  backlink: PropTypes.string,
  siteUrl: PropTypes.string,
}

export default Blog
