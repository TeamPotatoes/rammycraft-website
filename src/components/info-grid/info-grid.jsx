import React, { useState, useEffect } from "react"
import { MDCLinearProgress } from "@material/linear-progress"

import "./info-grid.scss"

const InfoGrid = () => {
  const [online, setOnline] = useState("--")
  const [max, setMax] = useState("--")
  const [playerList, setPlayerList] = useState([])
  const [version, setVersion] = useState("-.--.-")

  useEffect(() => {
    async function getServerStatus() {
      let response = await fetch(
        "https://api.mcsrvstat.us/2/mc.teampotatoes.com"
      )
      response = await response.json()

      setOnline(response.players.online)
      setMax(response.players.max)
      setPlayerList(response.players.list)
      setVersion(response.version)

      updateProgress(response.players.online / response.players.max)
    }

    function updateProgress(value) {
      linearProgress.progress = value

      linearProgress.determinate = true
    }

    const linearProgressEl = document.querySelector(".mdc-linear-progress")
    let linearProgress = new MDCLinearProgress(linearProgressEl)
    linearProgress.progress = 0
    linearProgress.determinate = false

    getServerStatus()
  }, [online])

  return (
    <React.Fragment>
      <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4 mdc-layout-grid__cell--span-12-tablet">
        <div className="center">
          <i className="material-icons fs-36">settings</i>
          <h1 className="mdc-typography--headline1 headline1">
            Server Details
          </h1>
          <div className="mb-1">
            <div className="mdc-typography--headline6">Server IP</div>
            <div className="mdc-typography--body1">
              rammycraft.teampotatoes.com
            </div>
            <div className="mdc-typography--body1">mc.teampotatoes.com</div>
          </div>

          <div className="mb-1">
            <div className="mdc-typography--headline6">Bedrock IP</div>
            <div className="mdc-typography--body1">
              bedrock.teampotatoes.com
            </div>
          </div>

          <div className="mb-1">
            <div className="mdc-typography--headline6">Server Version</div>
            <div className="mdc-typography--body1">{version}</div>
          </div>
        </div>
      </div>
      <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4 mdc-layout-grid__cell--span-12-tablet">
        <div className="center">
          <i className="material-icons fs-36">timer</i>
          <h1 className="mdc-typography--headline1 headline1">Server Status</h1>
          <div className="mb-1">
            <div className="mdc-typography--body1">The Server is online!</div>
          </div>

          <div className="mb-1">
            <div className="mdc-typography--body1" id="player">
              Players Online: {online} / {max}
            </div>
          </div>
        </div>

        <div role="progressbar" className="mdc-linear-progress progress">
          <div className="mdc-linear-progress__buffering-dots" />
          <div className="mdc-linear-progress__buffer" />
          <div className="mdc-linear-progress__bar mdc-linear-progress__primary-bar">
            <span className="mdc-linear-progress__bar-inner" />
          </div>
          <div className="mdc-linear-progress__bar mdc-linear-progress__secondary-bar">
            <span className="mdc-linear-progress__bar-inner" />
          </div>
        </div>
        <div className="playerlist">
          {playerList &&
            playerList.map((player) => {
              return (
                <img
                  src={`https://minotar.net/avatar/${player}/36.png`}
                  title={player}
                  alt={`${player} head icon`}
                  key={player}
                />
              )
            })}
        </div>
      </div>
      <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4 mdc-layout-grid__cell--span-12-tablet">
        <div className="center">
          <i className="material-icons fs-36">power</i>
          <h1 className="mdc-typography--headline1 headline1">
            Custom Scripted!
          </h1>
          <div className="mb-1">
            <div className="mdc-typography--body1">
              Server runs on custom made scripts that utilizes Skript&apos;s
              functionalities to achieve endless customizations!
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default InfoGrid
