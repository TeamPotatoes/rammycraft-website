import React from "react"
import PropTypes from "prop-types"

import Image from "../image"
import Card from "../card/card"

import "./portrait.scss"

const Portrait = props => (
  <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
    <div className=" portrait-container">
      <Image filename={props.filename} />
      <Card title={props.name} content={props.description}></Card>
    </div>
  </div>
)

Portrait.propTypes = {
  filename: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
}

export default Portrait
