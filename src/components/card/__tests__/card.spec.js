import React from "react"
import renderer from "react-test-renderer"
import Card from "../card"
describe("Card", () => {
  it("renders correctly", () => {
    const tree = renderer
      .create(<Card title="Test Card" content="Test Content" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
