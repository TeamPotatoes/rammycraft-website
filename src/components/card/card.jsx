import React from "react"
import PropTypes from "prop-types"

import "./card.scss"

const CardContent = props => (
  <div className="mdc-card mb-1 mdc-card--outlined">
    <div className="card-content">
      <h6 className="mdc-typography--headline6 headline6">{props.title}</h6>
      <p className="mdc-typography--body1">{props.content}</p>
    </div>
  </div>
)

CardContent.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
}

export default CardContent
