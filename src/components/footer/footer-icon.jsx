import React from "react"
import PropTypes from "prop-types"

import Image from "../image"

const FooterIcon = props => {
  return (
    <a
      href={props.href}
      target="_blank"
      rel="noopener noreferrer"
      className="footer-icon-container"
    >
      <Image filename={props.filename} />
    </a>
  )
}

FooterIcon.propTypes = {
  href: PropTypes.string,
  imgalt: PropTypes.string,
  filename: PropTypes.string,
}

export default FooterIcon
