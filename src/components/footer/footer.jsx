import React from "react"
import { StaticQuery, graphql } from "gatsby"

import "./footer.scss"
import FooterIcon from "./footer-icon"

function getURL(filename) {
  switch (filename) {
  case "facebook":
    return "https://www.facebook.com/TeamPotatoes/"
  case "facebookpage":
    return "https://www.facebook.com/groups/RammyCraft/"
  case "discord":
    return "https://discord.gg/0aMO9RA1BmdMUrmx"
  case "gitlab":
    return "https://gitlab.com/TeamPotatoes/rammycraft-website"
  default:
    return "#"
  }
}

const Footer = () => (
  <StaticQuery
    query={graphql`
      {
        socials: allFile(filter: { relativeDirectory: { eq: "social" } }) {
          edges {
            node {
              id
              name
              base
              relativeDirectory
              relativePath
            }
          }
        }
      }
    `}
    render={({ socials }) => {
      return (
        <React.Fragment>
          <footer>
            <div className="center">
              <div className="mdc-layout-grid">
                <div className="mdc-layout-grid__inner">
                  <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                    <span className="footer-text">
                      <div className="mdc-typography--headline6">
                        RammyCraft
                      </div>
                      <div style={{ display: "inline-flex" }}>
                        {socials.edges.map(({ node }) => {
                          return (
                            <FooterIcon
                              filename={node.base}
                              key={node.id}
                              href={getURL(node.name)}
                            />
                          )
                        })}
                      </div>

                      <div className="mdc-typography--subtitle1">
                        © {new Date().getFullYear()} RammyCraft
                      </div>

                      <div className="mdc-typography--subtitle2">
                        Website by{" "}
                        <a
                          href="https://gitlab.com/TomYaMee"
                          target="_blank"
                          rel="noopener noreferrer"
                          style={{ textDecoration: "none", color: "green" }}
                        >
                          TomYaMee
                        </a>
                        , Logo by{" "}
                        <a
                          href="https://www.facebook.com/scorpix.jason"
                          target="_blank"
                          rel="noopener noreferrer"
                          style={{ textDecoration: "none", color: "green" }}
                        >
                          Jason Yeap
                        </a>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </React.Fragment>
      )
    }}
  />
)

export default Footer
