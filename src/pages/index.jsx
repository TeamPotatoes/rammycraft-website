import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Card from "../components/card/card"
import Banner from "../components/banner/banner"
import InfoGrid from "../components/info-grid/info-grid"

const IndexPage = () => (
  <Layout isIndex={true}>
    <SEO
      title="Home"
      keywords={[`minecraft`, `rammycraft`, `survival`, `react`, `gatsby`]}
    />
    
    <Banner filename="1.png" alt="Banner" />
    <div className="container">
      <div className="mdc-layout-grid">
        <div className="mdc-layout-grid__inner">
          <InfoGrid />
          <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
            <Card
              title="Classic Survival"
              content="A classic survival server that offers little to no modification to the vanilla server, allowing users to experience Minecraft the vanilla way."
            />
            <Card
              title="Town Protection System"
              content="Tired of malicious user griefing your hard work? RammyCraft features custom made Town Protection System to protect your build. The system is highly customizable allowing user to set behaviors such as fire spread and friendly fire! The system also features a PR system allowing members from other town to access your town with configurable build and break permission."
            />
            <Card
              title="Chest Protection System"
              content="You can prevent your precious loot from getting stolen by other players or even your town members! The system allows you to define who you want to share the chest content with anytime you want. The system covers various chest type such as chest, trapped chest, shulker box, hopper, furnance and many more. It even helps protect your chest from those sneaky creepers that blows up behind you while you are not looking around!"
            />
          </div>
        </div>
      </div>
    </div>
  </Layout>
)

export default IndexPage
