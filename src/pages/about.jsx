import React from "react"
import { StaticQuery, graphql } from "gatsby"

import Portrait from "../components/portrait/portrait"
import Icon from "../components/icon/icon"
import Layout from "../components/layout"
import Article from "../components/article/article"
import SEO from "../components/seo"

function getURL(filename) {
  switch (filename) {
  case "gatsby":
    return "https://www.gatsbyjs.org/"
  case "mdc":
    return "https://material.io/"
  case "netlify":
    return "https://www.netlify.com/"
  case "react":
    return "https://reactjs.org/"
  case "webpack":
    return "https://webpack.js.org/"
  default:
    return
  }
}

const AboutPage = () => (
  <StaticQuery
    query={graphql`
      {
        icons: allFile(filter: { relativeDirectory: { eq: "techstack" } }) {
          edges {
            node {
              id
              name
              base
              relativeDirectory
              relativePath
            }
          }
        }
        teams: allFile(filter: { relativeDirectory: { eq: "team" } }) {
          edges {
            node {
              id
              name
              base
              relativeDirectory
              relativePath
            }
          }
        }
      }
    `}
    render={({ icons }) => {
      return (
        <Layout>
          <SEO
            title="About"
            keywords={[
              `minecraft`,
              `rammycraft`,
              `survival`,
              `react`,
              `gatsby`,
            ]}
          />
          <section className="about">
            <div className="container">
              <div className="mdc-layout-grid">
                <div className="mdc-layout-grid__inner">
                  <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                    <h6 className="mdc-typography--headline6 headline6">
                      About
                    </h6>
                    <Article>
                      <p>RammyCraft is a Minecraft Server based in Malaysia launched since 3rd of September 2012. RammyCraft is a completely free to play server with gameplay as close to vanilla survival as possible. Few tweaks such as town protection and chest protection are made to enhance players&apos; experience. Overall RammyCraft is a casual server that anyone can join and enjoy the vanilla experience with their friends.</p>

                      <p>RammyCraft was started as a passion project by TomYaMee to explore the aspect of game server management. The server was first created utilizing various ready-made solutions/plugins available on the market while TomYaMee was focusing on learning game server management. The server evolved into custom made server using Skript language at later stage as a way for TomYaMee to delve into the programming world. The official website was then developed using modern front-end framework utilizing React/Gatsby as TomYaMee gets more experienced with web development.</p>
                    </Article>
                  </div>

                  <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                    <h6 className="mdc-typography--headline6 headline6">
                      Website Built With
                    </h6>
                    {icons.edges.map(({ node }) => (
                      <Icon
                        href={getURL(node.name)}
                        id={node.id}
                        filename={node.base}
                        key={node.id}
                      />
                    ))}
                  </div>

                  <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                    <h6 className="mdc-typography--headline6 headline6">
                      Team
                    </h6>
                  </div>

                  <Portrait filename="tomyamee.png" name="TomYaMee" description="Co-founder of the server, currently active in development of the server"/>
                  <Portrait filename="atsukio.png" name="Atsukio" description="A friendly potato, currently active in development of the server"/>
                  <Portrait filename="deadlydes.png" name="DeadlyDes" description="Trusty admin currently assisting in administrating the server"/>
                  <Portrait filename="clearitz.png" name="CLEARITZ" description="Server police that helped maintain the peace. Currently inactive." />
                  <Portrait filename="grevale.png" name="Grevale" description="Best builder RammyCraft ever had. Helped built several POIs for previous RammyCraft servers. Currently inactive." />
                  <Portrait filename="viperkoro.png" name="ViperKoro" description="Another trusty admin that helped administrate previous servers. Currently inactive."/>
                  <Portrait filename="zarone.png" name="Zarone" description="The for fun admin. Helped with previous server management. Currently inactive."/>
                  <Portrait filename="gillisious.png" name="Gillisious" description="Used to help fund the server back in the early days. Currently inactive."/>
                  <Portrait filename="scorpix.png" name="Scorpix" description="The troll and fun admin. Helped the server back in the early days. Currently inactive." />
                  <Portrait filename="theexsky.png" name="TheExSky" description="Co-founder of the server, currently inactive."/>
                </div>
              </div>
            </div>
          </section>
        </Layout>
      )
    }}
  />
)

export default AboutPage
