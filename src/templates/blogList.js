import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import BlogList from "../components/blog-list/blog-list"
import GuideList from "../components/guide-list/guide-list"

const BlogPage = ({
  data: {
    allMarkdownRemark: { edges },
  },
  pageContext,
}) => (
  <Layout>
    <SEO
      title="Blog"
      keywords={[`minecraft`, `rammycraft`, `survival`, `react`, `gatsby`]}
    />
    <div className="container">
      <div className="mdc-layout-grid">
        <div className="mdc-layout-grid__inner">
          {pageContext.type === 'guide' ? 
            <GuideList content={edges} pageContext={pageContext}></GuideList>
            :
            <BlogList content={edges} pageContext={pageContext}></BlogList>
          }
        </div>
      </div>
    </div>
  </Layout>
)

export default BlogPage

export const pageQuery = graphql`
  query BlogQuery($skip: Int!, $limit: Int!, $type: String!) {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: { frontmatter: { layout: { eq: $type } } }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          id
          excerpt(pruneLength: 250)
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            thumbnail
            layout
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
