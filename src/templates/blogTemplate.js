import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Blog from "../components/blog/blog"
import SEO from "../components/seo"

import "./blogTemplate.scss"

function getThumbnail(path) {
  let filename = path.split(".")
  filename = filename[0].split("/")
  filename = filename[filename.length - 1]
  return filename
}

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark, site } = data // data.markdownRemark holds our post data
  const { frontmatter, html, excerpt, timeToRead, fields } = markdownRemark
  return (
    <Layout>
      <SEO
        title={frontmatter.title}
        keywords={[`minecraft`, `rammycraft`, `survival`, `react`, `gatsby`]}
        image={frontmatter.thumbnail}
        description={excerpt}
      />
      <Blog
        title={frontmatter.title}
        date={frontmatter.date}
        timeToRead={timeToRead}
        thumbnail={getThumbnail(frontmatter.thumbnail)}
        html={html}
        backlink={frontmatter.layout}
        siteUrl={`${site.siteMetadata.siteUrl}${fields.slug}`}
      />
    </Layout>
  )
}

export const pageQuery = graphql`
  query($title: String!, $date: Date!) {
    markdownRemark(
      frontmatter: { title: { eq: $title }, date: { eq: $date } }
    ) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY hh:mm A Z")
        title
        thumbnail
        layout
      }
      timeToRead
      excerpt
      fields {
        slug
      }
    }
    site {
      siteMetadata {
        siteUrl
      }
    }
  }
`
