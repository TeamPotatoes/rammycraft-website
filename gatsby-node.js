/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require("path")
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
}

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions

  const blogPostTemplate = path.resolve(`src/templates/blogTemplate.js`)
  const blogListTemplate = path.resolve(`src/templates/blogList.js`)

  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              title
              date
              layout
            }
            timeToRead
            excerpt
            fields {
              slug
            }
          }
        }
      }
    }
  `)
  const blogList = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
        filter: { frontmatter: { layout: { eq: "blog" } } }
      ) {
        edges {
          node {
            frontmatter {
              title
              date
              layout
            }
            timeToRead
            excerpt
            fields {
              slug
            }
          }
        }
      }
    }
  `)
  const updateList = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
        filter: { frontmatter: { layout: { eq: "update" } } }
      ) {
        edges {
          node {
            frontmatter {
              title
              date
              layout
            }
            timeToRead
            excerpt
            fields {
              slug
            }
          }
        }
      }
    }
  `)
  const guideList = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
        filter: { frontmatter: { layout: { eq: "guide" } } }
      ) {
        edges {
          node {
            frontmatter {
              title
              date
              layout
            }
            timeToRead
            excerpt
            fields {
              slug
            }
          }
        }
      }
    }
  `)
  if (result.errors) {
    return Promise.reject(result.errors)
  }
  if (blogList.errors) {
    return Promise.reject(result.errors)
  }
  if (updateList.errors) {
    return Promise.reject(result.errors)
  }
  if (guideList.errors) {
    return Promise.reject(result.errors)
  }

  const posts = result.data.allMarkdownRemark.edges
  posts.forEach(({ node }) => {
    const slug = node.fields.slug
    createPage({
      path: slug,
      component: blogPostTemplate,
      context: {
        title: node.frontmatter.title,
        date: node.frontmatter.date,
      }, // additional data can be passed via context
    })
  })
  
  const blogPosts = blogList.data.allMarkdownRemark.edges
  const postsPerPage = 6
  let numPages = Math.ceil(blogPosts.length / postsPerPage)
  Array.from({ length: numPages }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/blog` : `/blog/${i + 1}`,
      component: blogListTemplate,
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
        type: 'blog'
      },
    })
  })

  const updatePosts = updateList.data.allMarkdownRemark.edges
  numPages = Math.ceil(updatePosts.length / postsPerPage)
  Array.from({ length: numPages }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/update` : `/update/${i + 1}`,
      component: blogListTemplate,
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
        type: 'update'
      },
    })
  })

  const guidePosts = guideList.data.allMarkdownRemark.edges
  numPages = Math.ceil(guidePosts.length / postsPerPage)
  Array.from({ length: numPages }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/guide` : `/guide/${i + 1}`,
      component: blogListTemplate,
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
        type: 'guide'
      },
    })
  })
}
